# SeeModeWeb

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

## Installation

Requires node and npm

Run `npm install` 

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

If you are having trouble installing npm packages or starting the server, alternatively - you can just run `python -m SimpleHTTPServer 4200` in the `dist directory`.

The `dist directory` has the already built web app 



## Using web app

Click the `select file` button to select file. The only valid input formats are `.vti` files

On the `Image Viewer` and `Slicer` tab, you can enable / disable the UI View controller or the Multi slice controller. They can also be enabled at the same time

In the `segmentation` tab, the image file is only sent to the server for processing when the drag on the threshold slider has ended/stopped (ie it will not continously send segmentation processing requests to the server during dragging)

On the `image seeding` tab, use the mouse right click on the left render window to select a point. Only two points can be selected. Use the trash icon button next to the point to erase it - in order to select a new point.

The right render window on the `image seeding` tab displays the result of the seed based segmentation whenever two points are selected.

The radius of the points selected can be changed using the point radius slider