import { Directive, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from "rxjs"
import { filter, map, debounceTime, withLatestFrom } from "rxjs/operators"
import { combineLatest } from "rxjs/observable/combineLatest"
import { RestApiService } from "../../services/rest-api.service"


import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import vtkXMLPolyDataReader from 'vtk.js/Sources/IO/XML/XMLPolyDataReader';


@Directive({
  selector: 'image-seeder',
  exportAs: 'ImageSeeder'
})
export class ImageSeederDirective implements OnInit {

  fileObserv = new BehaviorSubject(null);
  seedObserv = new BehaviorSubject(null);
  screenObserv = new BehaviorSubject(null);

  @Input('outScreen') set outScreen(screen) {
  	this.screenObserv.next(screen);
  }

  @Input('file') set file(file:File) {
  	this.fileObserv.next(file);
  }

  @Input('seedValues') set seedValues(seedValues:any[]) {
  	this.seedObserv.next(seedValues);
  }

  private screenRenderer;
  private actors = [];

  constructor(private restApi:RestApiService) { }

  ngOnInit() {

  	let validSeedFilter = filter(t=>this.seedValuesValid(t));
  	let seedValueMapper = map(t=>JSON.parse(JSON.stringify(t)));
  	let noNull = filter(t=>t!=null);

  	this.seedObserv.subscribe(seedValues=>{
  		//remove previous actors if not valid seed data
  		if(this.screenRenderer && !this.seedValuesValid(seedValues))
  		{
  			this.actors.forEach(actor=>this.screenRenderer.getRenderer().removeActor(actor))
  			this.screenRenderer.getRenderWindow().render();
  		}
  	})

  	//debounce to settle on final values as changes occur

  	this.seedObserv.pipe(validSeedFilter,seedValueMapper)
  	.pipe(withLatestFrom(combineLatest(this.fileObserv.pipe(noNull),this.screenObserv.pipe(noNull))))
  	.map(([seedValues,params])=>{
  		 params.push(seedValues);
  		 return params;
  	})
  	.subscribe(([file,screen,seedValues]:any[])=>{

  		this.restApi.applySeedToFile(file,seedValues[0],seedValues[1])
  		.subscribe(binaryFile=>{

  			let objReader = vtkXMLPolyDataReader.newInstance();
	        objReader.parseAsArrayBuffer(binaryFile);

	        //remove previous actors
	        if(this.screenRenderer)
	        	this.actors.forEach(actor=>this.screenRenderer.getRenderer().removeActor(actor))
	      
	        this.screenRenderer = screen;

	        this.actors = [];
	        
	        let nbOutputs = objReader.getNumberOfOutputPorts();
	        for (let idx = 0; idx < nbOutputs; idx++) {
	          let source = objReader.getOutputData(idx);
	          let mapper = vtkMapper.newInstance();
	          let actor = vtkActor.newInstance();
	          actor.setMapper(mapper);
	          mapper.setInputData(source);
	          this.actors.push(actor);
	        }

	        
	        this.actors.forEach(actor=>this.screenRenderer.getRenderer().addActor(actor))

        	this.screenRenderer.getRenderer().resetCamera();
    		this.screenRenderer.getRenderWindow().render();
  		})
  	})
  }


  seedValuesValid(values):boolean {
  	return values && values[0] && values[1];
  }

}
