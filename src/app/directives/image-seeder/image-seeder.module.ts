import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageSeederDirective } from './image-seeder.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ImageSeederDirective],
  exports: [ImageSeederDirective]
})
export class ImageSeederModule { }
