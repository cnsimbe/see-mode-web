import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http"
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { MaterialModule } from "./material/material.module";

import { RestApiService } from "./services/rest-api.service";


import { ViewerSliderModule } from './viewer-slicer/viewer-slicer.module'
import { SegmentViewerModule } from './segment-viewer/segment-viewer.module';
import { SeedViewerModule } from './seed-viewer/seed-viewer.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MaterialModule,
    ViewerSliderModule,
    SegmentViewerModule,
    SeedViewerModule,
    HttpClientModule
  ],
  providers: [RestApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
