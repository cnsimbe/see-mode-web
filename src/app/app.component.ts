import { Component, OnInit, ChangeDetectorRef  } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';
  file:File;
  controlWidth = 300;

  fileElement = document.createElement("input")

  selectedIndex = 0;
  constructor(private changeRef:ChangeDetectorRef) {
  
  }

  ngOnInit() {
    this.fileElement.type = "file"
    this.fileElement.multiple = false
  }

  chooseFile() {
  	this.fileElement.click()
  	this.fileElement.onchange = ()=>{
  		this.file=this.fileElement.files[0]
  		this.changeRef.detectChanges()
  	}
  }




}
