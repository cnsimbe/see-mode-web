import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms" 
import { CommonModule } from '@angular/common';
import { SegmentViewerComponent } from "./segment-viewer.component"
import { MaterialModule } from "../material/material.module"

import { VolumeViewerModule } from "../volume-viewer/volume-viewer.module"
import { SegmentControlModule } from "../segment-control/segment-control.module"


@NgModule({
  imports: [
    CommonModule,
    SegmentControlModule,
    VolumeViewerModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [SegmentViewerComponent],
  exports : [SegmentViewerComponent]
})
export class SegmentViewerModule { }
