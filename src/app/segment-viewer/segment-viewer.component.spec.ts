import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SegmentViewerComponent } from './segment-viewer.component';

describe('SegmentViewerComponent', () => {
  let component: SegmentViewerComponent;
  let fixture: ComponentFixture<SegmentViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SegmentViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
