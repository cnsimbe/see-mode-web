import { Component, ViewChild, ElementRef, ChangeDetectorRef ,OnInit, Input } from '@angular/core';
import { ImageSeederDirective } from "../directives/image-seeder/image-seeder.directive"

@Component({
  selector: 'seed-viewer',
  templateUrl: './seed-viewer.component.html',
  styleUrls: ['./seed-viewer.component.css']
})
export class SeedViewerComponent implements OnInit {

  @Input('file') file:File;
  selectedIndex = 0;
  @ViewChild('container1') containerRef:ElementRef;


  resizeListener:any;
  controlWidth = 300;

  constructor(private changeRef:ChangeDetectorRef) {
  }


  ngOnDestroy() {
    window.removeEventListener('resize',this.resizeListener);
  }

  ngOnInit() {

    let context = this;
    this.resizeListener = () => this.updateControlWidth(context);

    window.addEventListener('resize', this.resizeListener);
    
    this.updateControlWidth(this);
  }

  updateControlWidth(context:SeedViewerComponent) {
    context.controlWidth = context.containerRef.nativeElement.clientWidth /3;
    context.changeRef.detectChanges()
  }


}
