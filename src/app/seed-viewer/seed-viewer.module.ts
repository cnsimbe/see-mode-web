import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms" 
import { CommonModule } from '@angular/common';
import { SeedViewerComponent } from "./seed-viewer.component"
import { MaterialModule } from "../material/material.module"

import { VolumeControlModule } from "../volume-control/volume-control.module"
import { VolumeViewerModule } from "../volume-viewer/volume-viewer.module"
import { SeedControlModule } from "../seed-control/seed-control.module"
import { VolumeSlicerModule } from "../volume-slicer/volume-slicer.module"
import { ImageSeederModule } from "../directives/image-seeder/image-seeder.module"

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    VolumeControlModule,
    VolumeViewerModule,
    SeedControlModule,
    VolumeSlicerModule,
    ImageSeederModule
  ],
  declarations: [SeedViewerComponent],
  exports: [ SeedViewerComponent ]
})
export class SeedViewerModule { }
