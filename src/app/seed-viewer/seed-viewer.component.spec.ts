import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeedViewerComponent } from './seed-viewer.component';

describe('SeedViewerComponent', () => {
  let component: SeedViewerComponent;
  let fixture: ComponentFixture<SeedViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeedViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeedViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
