import { NgModule } from '@angular/core';
import {MatButtonModule, MatButtonToggleModule ,MatIconModule ,MatTabsModule, MatSelectModule, MatSliderModule, MatListModule , MatFormFieldModule, MatInputModule ,MatCheckboxModule} from '@angular/material';

@NgModule({
  imports: [
  	MatButtonModule,
  	MatFormFieldModule,
  	MatInputModule,
  	MatCheckboxModule,
    MatSliderModule,
    MatListModule,
    MatSelectModule,
    MatTabsModule,
    MatIconModule,
    MatButtonToggleModule,
  ],
  exports: [
  	MatButtonModule,
  	MatFormFieldModule,
  	MatInputModule,
  	MatCheckboxModule,
    MatSliderModule,
    MatListModule,
    MatSelectModule,
    MatTabsModule,
    MatIconModule,
    MatButtonToggleModule
  ]
})
export class MaterialModule { }
