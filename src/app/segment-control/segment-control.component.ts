import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from "../services/rest-api.service"

import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import vtkXMLPolyDataReader from 'vtk.js/Sources/IO/XML/XMLPolyDataReader';

@Component({
  selector: 'segment-control',
  templateUrl: './segment-control.component.html',
  styleUrls: ['./segment-control.component.css']
})
export class SegmentControlComponent implements OnInit {

  private screenRenderer;
  private actors = [];
  private isEnabled = true;
  private segmentValue = 480;
  private file:File;


  @Input('file') set setFile(file:File) {

  	if(!file || this.file==file)
  		return;

  	this.file = file;

  	this.updateSegment(this.segmentValue);
  }

  constructor(private restApi:RestApiService) { }

  ngOnInit() {
  }


  @Input('screenRenderer') set setRenderScreen(renderScreen) {

  	if(!renderScreen)
  		return;

  	if(this.screenRenderer!=renderScreen) 
  	{
  		this.screenRenderer = renderScreen;

  		if(this.isEnabled==true)
  			this.actors.forEach(actor=>this.screenRenderer.getRenderer().addActor(actor))
  	}


    this.screenRenderer.getRenderWindow().render();

  }




  enable() {

  	this.isEnabled = true;

    if(!this.screenRenderer)
      return;

    let renderer = this.screenRenderer.getRenderer()
    
    this.actors.forEach(actor=>this.screenRenderer.getRenderer().addActor(actor))

    this.screenRenderer.getRenderWindow().render()

  }
 
  disable() {

  	this.isEnabled = false;

    if(!this.screenRenderer)
      return;

    let renderer = this.screenRenderer.getRenderer()
    
    this.actors.forEach(actor=>this.screenRenderer.getRenderer().removeActor(actor))

    this.screenRenderer.getRenderWindow().render()

  }

  updateSegment(value:number) {

    this.restApi.segmentFile(this.file,this.segmentValue).subscribe(binaryResult=> {
        let objReader = vtkXMLPolyDataReader.newInstance();
        objReader.parseAsArrayBuffer(binaryResult);


        // remove previous actors
        if(this.screenRenderer)
        	this.actors.forEach(actor=>this.screenRenderer.getRenderer().removeActor(actor))

        this.actors = [];
        
        let nbOutputs = objReader.getNumberOfOutputPorts();
        for (let idx = 0; idx < nbOutputs; idx++) {
          let source = objReader.getOutputData(idx);
          let mapper = vtkMapper.newInstance();
          let actor = vtkActor.newInstance();
          actor.setMapper(mapper);
          mapper.setInputData(source);
          this.actors.push(actor);
        }

        if(this.isEnabled==true && this.screenRenderer)
        	this.actors.forEach(actor=>this.screenRenderer.getRenderer().addActor(actor))


        if(this.screenRenderer)
        {
        	this.screenRenderer.getRenderer().resetCamera();
    		  this.screenRenderer.getRenderWindow().render()

          setTimeout(()=>{
            this.screenRenderer.getRenderer().resetCamera();
            this.screenRenderer.getRenderWindow().render()
          },330)

        }
        

      });

  }

}
