import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms" 
import { CommonModule } from '@angular/common';
import { SegmentControlComponent } from "./segment-control.component"
import { MaterialModule } from "../material/material.module"

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [SegmentControlComponent],
  exports: [SegmentControlComponent]
})
export class SegmentControlModule { }
