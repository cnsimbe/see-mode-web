import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeedControlComponent } from './seed-control.component';

describe('SeedControlComponent', () => {
  let component: SeedControlComponent;
  let fixture: ComponentFixture<SeedControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeedControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeedControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
