import { NgModule, TemplateRef } from '@angular/core';
import { FormsModule } from "@angular/forms" 
import { CommonModule } from '@angular/common';
import { SeedControlComponent } from "./seed-control.component"
import { MaterialModule } from "../material/material.module"

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  exports: [SeedControlComponent],
  declarations: [SeedControlComponent]
})
export class SeedControlModule { }
