import { Component, ChangeDetectorRef ,OnInit, Output ,Input, EventEmitter } from '@angular/core';
import vtkCellPicker from 'vtk.js/Sources/Rendering/Core/CellPicker';
import vtkPicker from 'vtk.js/Sources/Rendering/Core/Picker';
import vtkPointPicker from 'vtk.js/Sources/Rendering/Core/PointPicker';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import { VolumeSlicerComponent } from "../volume-slicer/volume-slicer.component"
import vtkSphereSource from 'vtk.js/Sources/Filters/Sources/SphereSource';



import { RestApiService } from "../services/rest-api.service"
import { BehaviorSubject } from "rxjs"
import { combineLatest } from 'rxjs/observable/combineLatest';
import {  filter } from "rxjs/operators"

@Component({
  selector: 'seed-control',
  templateUrl: './seed-control.component.html',
  styleUrls: ['./seed-control.component.css']
})
export class SeedControlComponent implements OnInit {


  private pointRadius = 0.4;
  private seedValues = [];
  private sphereActors = [];
  private sphereSources = [];


  private inScreen;

  private picker = vtkPointPicker.newInstance();
  private sliceObserv = new BehaviorSubject(null);

  private inScreenObserv = new BehaviorSubject(null);
  private dataSourceObserv = new BehaviorSubject(null);


  @Input('slicer') set setSlicer(slicer) {

    this.sliceObserv.next(slicer)

  }


  @Input('source') set setSource(source) {

  	this.dataSourceObserv.next(source)

  }

  @Input('inScreen') set setInScreenRenderer(screenRender) {

    this.inScreenObserv.next(screenRender);

  }

  @Output('seedValues') seedValuesEmitter = new EventEmitter();

  constructor(private changeRef:ChangeDetectorRef, private restApi:RestApiService) { }

  ngOnInit() {


    let noNull = filter(t=>t!=null);
    combineLatest(this.dataSourceObserv.pipe(noNull).distinctUntilChanged(),this.inScreenObserv.pipe(noNull).distinctUntilChanged(),this.sliceObserv.pipe(noNull).distinctUntilChanged())
    .subscribe(([source,screenRenderer,slicer]:any)=>{

      window["sd"] = this;

     
      let renderer = screenRenderer.getRenderer();
      let renderWindow = screenRenderer.getRenderWindow();
      let sliceActors = [ slicer.imageActorI, slicer.imageActorJ, slicer.imageActorK  ];
      sliceActors.forEach(sliceActor=>renderer.addActor(sliceActor));
     
      this.picker.setPickFromList(1);
      this.picker.initializePickList();

      sliceActors.forEach(slideActor=>this.picker.addPickList(slideActor))


      if(this.seedValues[0])
        this.removeSeed(this.seedValues[0])

      if(this.seedValues[1])
        this.removeSeed(this.seedValues[1])



     

      if(screenRenderer!=this.inScreen)
      {
        this.inScreen = screenRenderer;
        this.initializeListener(screenRenderer);
      }



    })
     
  }


  initializeListener(screenRenderer) {


    let renderer = screenRenderer.getRenderer();
    let renderWindow = screenRenderer.getRenderWindow();


    renderWindow.getInteractor().onRightButtonPress((callData) => {

        if (renderer !== callData.pokedRenderer) {
          return;
        }

        const pos = callData.position;
        const point = [pos.x, pos.y, pos.z];
        let result = this.picker.pick(point, renderer);

        if (this.picker.getActors().length > 0) {

          const pickedPointId = this.picker.getPointId();

          /*
            Get the closest point selected visible to the current from the viewpoint of the camera
          */


          let cameraPosition = renderer.getActiveCamera().getDirectionOfProjection();
          let pickedPoints = this.picker.getPickedPositions().sort((a,b)=>a[2]>b[2] ? -1 : 1);
         

          let pickedPoint = cameraPosition[2]<=0 ? pickedPoints[0] : pickedPoints[pickedPoints.length-1];

          

          if(pickedPoint)
            this.addSeedValue(pickedPoint);



        }
        renderWindow.render();
      });

  }


  seedString(seedValue) {
    let point = seedValue[0];
    let intensity = seedValue[1]
    return `[${Math.floor(point[0])}, ${Math.floor(point[1])}, ${Math.floor(point[2])}] Intensity: ${intensity}`;
  }


  updatePointRadius(radius:number) {
    this.sphereSources.filter(source=>source!=null).forEach(source=>source.setRadius(radius))
    this.sphereSources.forEach((sphere, index)=>{
      if(sphere)
        this.sphereActors[index].getMapper().setInputData(sphere.getOutputData())
    })

    if(this.inScreen)
    {
      let renderWindow = this.inScreen.getRenderWindow();
      renderWindow.render();

    }
  }

  removeSeed(seedValue) {

    let index = this.seedValues.indexOf(seedValue);
    if(index>=0) {
      this.seedValues[index] = null;
      let renderer = this.inScreen.getRenderer();
      let renderWindow = this.inScreen.getRenderWindow();
      renderer.removeActor(this.sphereActors[index])

      this.sphereActors[index] = null;
      this.sphereSources[index]= null;

      this.seedValuesEmitter.emit(this.seedValues);

      renderWindow.render();
    }
    
  }

  addSeedValue(pickedPoint) :boolean {


    if(this.seedValues[0] && this.seedValues[1])
      return false;

    let indexSelect = this.seedValues[0] ? 1 : 0;


    let renderer = this.inScreen.getRenderer();
    let renderWindow = this.inScreen.getRenderWindow();

    const sphere = vtkSphereSource.newInstance();
    sphere.setCenter(pickedPoint);
    sphere.setRadius(this.pointRadius);
    const sphereMapper = vtkMapper.newInstance();
    sphereMapper.setInputData(sphere.getOutputData());
    const sphereActor = vtkActor.newInstance();
    sphereActor.setMapper(sphereMapper);
    sphereActor.getProperty().setColor(0.0, 1.0, 0.0);
    renderer.addActor(sphereActor);


    this.sphereActors[indexSelect] = sphereActor;
    this.sphereSources[indexSelect] = sphere;

    let source = this.dataSourceObserv.getValue();
    let extent = source.getExtent();
    let dataArray = source.getPointData().getScalars() || source.getPointData().getArrays()[0];
    let iExtent = (extent[1] - extent[0]) + 1;
    let jExtent = (extent[3] - extent[2]) + 1;
    let kExtent = (extent[5] - extent[4]) + 1;

    let position = []
    source.worldToIndex(pickedPoint,position)

    position =  position
                .map(p=>Math.round(p))
                .map((p,index)=>Math.max(p, extent[index*2]))
                .map((p,index)=>Math.min(p, extent[index*2 + 1]));

    let deltaI = position[0] - extent[0];
    let deltaJ = position[1] - extent[2];
    let deltaK = position[2] - extent[4];

    let index = deltaI + deltaJ*iExtent + deltaK*iExtent*jExtent;

    let seedValue = [position, dataArray.getData()[index] ];

    this.seedValues[indexSelect] = seedValue;

    this.seedValuesEmitter.emit(this.seedValues);


    return true;

  }



}
