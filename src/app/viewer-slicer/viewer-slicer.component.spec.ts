import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewerSlicerComponent } from './viewer-slicer.component';

describe('ViewerSlicerComponent', () => {
  let component: ViewerSlicerComponent;
  let fixture: ComponentFixture<ViewerSlicerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewerSlicerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerSlicerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
