import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms" 
import { CommonModule } from '@angular/common';
import { ViewerSlicerComponent } from "./viewer-slicer.component"
import { MaterialModule } from "../material/material.module"

import { VolumeViewerModule } from "../volume-viewer/volume-viewer.module"
import { VolumeControlModule } from "../volume-control/volume-control.module"
import { VolumeSlicerModule } from "../volume-slicer/volume-slicer.module"

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    VolumeControlModule,
    VolumeSlicerModule,
    VolumeViewerModule
  ],
  declarations: [ViewerSlicerComponent],
  exports: [

  	ViewerSlicerComponent

  ]
})
export class ViewerSliderModule { }
