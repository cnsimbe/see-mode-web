import { Component, OnInit, Input, OnDestroy ,ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';


@Component({
  selector: 'viewer-slicer',
  templateUrl: './viewer-slicer.component.html',
  styleUrls: ['./viewer-slicer.component.css']
})
export class ViewerSlicerComponent implements OnInit, OnDestroy {


  controlWidth = 300;

  @Input('file') file:File;
  @ViewChild('container1') containerRef:ElementRef;

  resizeListener:any;

  constructor(private changeRef:ChangeDetectorRef) {
  	

  }


  ngOnDestroy() {
    window.removeEventListener('resize',this.resizeListener);
  }

  ngOnInit() {

    let context = this;
    this.resizeListener = () => this.updateControlWidth(context);

    window.addEventListener('resize', this.resizeListener);
    
    this.updateControlWidth(this);
  }

  updateControlWidth(context:ViewerSlicerComponent) {
    context.controlWidth = context.containerRef.nativeElement.clientWidth /3;
    context.changeRef.detectChanges()
  }


}
