import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSliderChange, MatSlider } from "@angular/material"

import vtkImageMapper from '../mapper/ImageMapper';
import vtkImageSlice from 'vtk.js/Sources/Rendering/Core/ImageSlice';

@Component({
  selector: 'volume-slicer',
  templateUrl: './volume-slicer.component.html',
  styleUrls: ['./volume-slicer.component.css']
})
export class VolumeSlicerComponent implements OnInit {

  constructor() { }


  private isEnabled = true;
  private imageMapperI = vtkImageMapper.newInstance();
  private imageMapperJ = vtkImageMapper.newInstance();
  private imageMapperK = vtkImageMapper.newInstance();

  imageActorI = vtkImageSlice.newInstance();
  imageActorJ = vtkImageSlice.newInstance();
  imageActorK = vtkImageSlice.newInstance();

  private screenRenderer;


  @ViewChild('slideI') slideI : MatSlider;
  @ViewChild('slideJ') slideJ : MatSlider;
  @ViewChild('slideK') slideK : MatSlider;
  @ViewChild('slideCL') slideCL : MatSlider;
  @ViewChild('slideCW') slideCW : MatSlider;




  ngOnInit() {

  	//this.imageMapperI.setISlice(30);
  	//this.imageMapperJ.setJSlice(30);
  	//this.imageMapperK.setKSlice(30);

  	this.imageActorI.setMapper(this.imageMapperI);
  	this.imageActorJ.setMapper(this.imageMapperJ);
  	this.imageActorK.setMapper(this.imageMapperK);


  }


  enable() {

    this.isEnabled = true;

    if(!this.screenRenderer)
      return;

    let renderer = this.screenRenderer.getRenderer()
    
    renderer.addActor(this.imageActorI);
    renderer.addActor(this.imageActorJ);
    renderer.addActor(this.imageActorK);


    this.screenRenderer.getRenderWindow().render()


  }

  disable() {


    this.isEnabled = false;

    if(!this.screenRenderer)
      return;

    let renderer = this.screenRenderer.getRenderer()
    
    renderer.removeActor(this.imageActorI);
    renderer.removeActor(this.imageActorJ);
    renderer.removeActor(this.imageActorK);


    this.screenRenderer.getRenderWindow().render()
  }


  setDataSource(screenRenderer,source) {
  	if(this.screenRenderer != screenRenderer)
  	{
      this.screenRenderer = screenRenderer;

      if(this.isEnabled==true)
      {
        let renderer = screenRenderer.getRenderer()
        renderer.addActor(this.imageActorI);
        renderer.addActor(this.imageActorJ);
        renderer.addActor(this.imageActorK);
      }
      
  		
  	}

  	this.imageMapperI.setInputData(source);
  	this.imageMapperJ.setInputData(source);
  	this.imageMapperK.setInputData(source);

  	const dataRange = source
      .getPointData()
      .getScalars()
      .getRange();
    const extent = source.getExtent();


    let slideIds = ["i", "j", "k"];

    [this.slideI, this.slideJ, this.slideK].forEach((slider,index)=>{
    	slider.min = extent[index * 2 + 0];
    	slider.max = extent[index * 2 + 1];
    	slider.value = (slider.min + slider.max)/2;

      this.updateSlide(slideIds[index],{value:slider.value,source:slider},true);
    });


    [this.slideCL, this.slideCW].forEach((slider,index)=>{
      slider.max = dataRange[1];
      slider.value = dataRange[1];
    });

    this.slideCL.value = (dataRange[0] + dataRange[1]) / 2;

    this.updateCL({value:this.slideCL.value,source:this.slideCL},false);
    this.updateCW({value:this.slideCW.value,source:this.slideCW},false);


    //this.screenRenderer.getRenderer().resetCamera();
    //this.screenRenderer.getRenderWindow().render()
  }

  updateSlide(slideId:string,$event:MatSliderChange, render=true) {
    
    if(!this.screenRenderer)
      return;

    let slideValue = Math.floor($event.value);


    if(slideId=='i')
      this.imageActorI.getMapper().setISlice(slideValue);
  
    if(slideId=='j')
      this.imageActorJ.getMapper().setJSlice(slideValue);

    if(slideId=='k')
      this.imageActorK.getMapper().setKSlice(slideValue);

    if(render==true)
      this.screenRenderer.getRenderWindow().render();
  }

  updateCL($event:MatSliderChange,render=true) {

    if(!this.screenRenderer)
      return;

    let colorLevel = $event.value;

    this.imageActorI.getProperty().setColorLevel(colorLevel);
    this.imageActorJ.getProperty().setColorLevel(colorLevel);
    this.imageActorK.getProperty().setColorLevel(colorLevel);
    
    if(render==true)
      this.screenRenderer.getRenderWindow().render();
  }


  updateCW($event:MatSliderChange,render=true) {

    if(!this.screenRenderer)
      return;

    let colorLevel = $event.value;

    this.imageActorI.getProperty().setColorWindow(colorLevel);
    this.imageActorJ.getProperty().setColorWindow(colorLevel);
    this.imageActorK.getProperty().setColorWindow(colorLevel);
    

    if(render==true)
      this.screenRenderer.getRenderWindow().render();

  }


}
