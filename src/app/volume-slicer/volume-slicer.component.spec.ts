import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolumeSlicerComponent } from './volume-slicer.component';

describe('VolumeSlicerComponent', () => {
  let component: VolumeSlicerComponent;
  let fixture: ComponentFixture<VolumeSlicerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolumeSlicerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolumeSlicerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
