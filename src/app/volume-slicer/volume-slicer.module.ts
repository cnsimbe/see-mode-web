import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VolumeSlicerComponent } from "./volume-slicer.component"
import { MaterialModule } from "../material/material.module"

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [VolumeSlicerComponent],
  exports: [VolumeSlicerComponent]
})
export class VolumeSlicerModule { }
