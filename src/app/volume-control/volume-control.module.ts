import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms" 
import { CommonModule } from '@angular/common';
import { VolumeControlComponent } from "./volume-control.component"
import { MaterialModule } from "../material/material.module"

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [VolumeControlComponent],
  exports: [VolumeControlComponent]
})
export class VolumeControlModule { }
