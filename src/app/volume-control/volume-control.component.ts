import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import vtkVolumeController from 'vtk.js/Sources/Interaction/UI/VolumeController';


import vtkColorMaps from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction/ColorMaps';
import vtkPiecewiseGaussianWidget from 'vtk.js/Sources/Interaction/Widgets/PiecewiseGaussianWidget';

import vtkVolume from 'vtk.js/Sources/Rendering/Core/Volume';
import vtkVolumeMapper from 'vtk.js/Sources/Rendering/Core/VolumeMapper';
import vtkBoundingBox from 'vtk.js/Sources/Common/DataModel/BoundingBox';
import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction';
import vtkFullScreenRenderWindow from 'vtk.js/Sources/Rendering/Misc/FullScreenRenderWindow';
import vtkPiecewiseFunction from 'vtk.js/Sources/Common/DataModel/PiecewiseFunction';

@Component({
  selector: 'volume-control',
  templateUrl: './volume-control.component.html',
  styleUrls: ['./volume-control.component.css']
})
export class VolumeControlComponent implements OnInit {


  private actor = vtkVolume.newInstance();
  private mapper = vtkVolumeMapper.newInstance();
  private lookupTable = vtkColorTransferFunction.newInstance();
  private piecewiseFunction = vtkPiecewiseFunction.newInstance();
  private screenRenderer;
  private isEnabled =  true;
  private space_value = 0.4;
  private edge_value = 0.2;
  private widget;

  @ViewChild('container') container:ElementRef;
  constructor() { }
  private width = 400
  private height = 150;

  private use_shadow:boolean = true;
  private preset_color:string = 'erdc_rainbow_bright'

  colorOptions = vtkColorMaps.rgbPresetNames;

  //private vCtrl = vtkVolumeController.newInstance({
    //size: [this.width, this.height],
  //  rescaleColorMap: true,
  //});

  @Input('width') set setWidth(width:number) {
  	if(width==0 || width) {
  		this.width = width;
      if(this.widget)
  		  this.widget.setSize(this.width,this.height)

  	}
  }

  @Input('height') set setHeight(height:number) {
  	if(height==0 || height) {
  		this.height = height;
      if(this.widget)
  		  this.widget.setSize(this.width,this.height)
  	}
  }

  ngOnInit() {



    let containerElem = this.container.nativeElement as HTMLElement;

      // Pipeline handling
    this.actor.setMapper(this.mapper);

    

    //Configuration
    this.actor.getProperty().setRGBTransferFunction(0, this.lookupTable);
    this.actor.getProperty().setScalarOpacity(0, this.piecewiseFunction);
    this.actor.getProperty().setInterpolationTypeToLinear();
    this.actor.getProperty().setGradientOpacityMinimumValue(0, 0);


    this.actor.getProperty().setShade(true);
    this.actor.getProperty().setUseGradientOpacity(0, true);
      // - generic good default
    this.actor.getProperty().setGradientOpacityMinimumOpacity(0, 0.0);
    this.actor.getProperty().setGradientOpacityMaximumOpacity(0, 1.0);
    this.actor.getProperty().setAmbient(0.2);
    this.actor.getProperty().setDiffuse(0.7);
    this.actor.getProperty().setSpecular(0.3);
    this.actor.getProperty().setSpecularPower(8.0);

    this.widget =   vtkPiecewiseGaussianWidget.newInstance({
      numberOfBins: 256,
      size: [this.width, this.height],
    });

    this.widget.updateStyle({
      backgroundColor: 'rgba(255, 255, 255, 0.6)',
      histogramColor: 'rgba(100, 100, 100, 0.5)',
      strokeColor: 'rgb(0, 0, 0)',
      activeColor: 'rgb(255, 255, 255)',
      handleColor: 'rgb(50, 150, 50)',
      buttonDisableFillColor: 'rgba(255, 255, 255, 0.5)',
      buttonDisableStrokeColor: 'rgba(0, 0, 0, 0.5)',
      buttonStrokeColor: 'rgba(0, 0, 0, 1)',
      buttonFillColor: 'rgba(255, 255, 255, 1)',
      strokeWidth: 2,
      activeStrokeWidth: 3,
      buttonStrokeWidth: 1.5,
      handleWidth: 3,
      iconSize: 0,
      padding: 10,
    });

    this.widget.addGaussian(0.5, 1.0, 0.5, 0.5, 0.4);

    //this.widget.setDataArray(dataArray.getData());
    this.widget.setColorTransferFunction(this.lookupTable);
    this.widget.applyOpacity(this.piecewiseFunction);
    this.widget.setContainer(containerElem);
    this.widget.bindMouseListeners();

    this.widget.onOpacityChange(() => {
      this.widget.applyOpacity(this.piecewiseFunction);
      this.updateColorMapPreset(this.preset_color);

      if(!this.screenRenderer)
        return;
      let renderWindow = this.screenRenderer.getRenderWindow();

      if (!renderWindow.getInteractor().isAnimating()) {
        renderWindow.render();
      }
    });

    this.widget.onAnimation((start) => {

      if(!this.screenRenderer)
        return;

      let renderWindow = this.screenRenderer.getRenderWindow();

      if (start) {
         renderWindow.getInteractor().requestAnimation(this.widget);
      } else {
         renderWindow.getInteractor().cancelAnimation(this.widget);
         renderWindow.render();
      }
    });

    this.lookupTable.onModified(() => {
      this.widget.render();

      if(!this.screenRenderer)
        return;

       let renderWindow = this.screenRenderer.getRenderWindow();

      if (!renderWindow.getInteractor().isAnimating()) {
         renderWindow.render();
      }
    });
    this.refreshValues();    
  }



  refreshValues() {

    this.updateUseShadow(this.use_shadow);
    this.updateColorMapPreset(this.preset_color);
    this.updateSpacing(this.space_value);
    this.updateEdgeGradient(this.edge_value);
    this.widget.render();
  }


  enable() {

    this.isEnabled = true;

    if(!this.screenRenderer)
      return;

    let renderer = this.screenRenderer.getRenderer()
    
    renderer.addActor(this.actor);
    this.screenRenderer.getRenderWindow().render()




  }

  disable() {


    this.isEnabled = false;

    if(!this.screenRenderer)
      return;

    let renderer = this.screenRenderer.getRenderer()
    
    renderer.removeActor(this.actor);


    this.screenRenderer.getRenderWindow().render()
  }

  setDataSource(screenRenderer, source) {


    window["e"] = source;

    
   

    let dataArray = source.getPointData().getScalars() || source.getPointData().getArrays()[0];
    let dataRange = dataArray.getRange();

    this.mapper.setInputData(source);

    let sampleDistance = 0.7 * Math.sqrt(source.getSpacing().map((v) => v * v).reduce((a, b) => a + b, 0));
    this.mapper.setSampleDistance(sampleDistance);
    this.actor.getProperty().setScalarOpacityUnitDistance(0,vtkBoundingBox.getDiagonalLength(source.getBounds()) /Math.max(...source.getDimensions()));
    this.actor.getProperty().setGradientOpacityMaximumValue(0, (dataRange[1] - dataRange[0]) * 0.05)

    if(this.screenRenderer!=screenRenderer)
    {
      
      this.screenRenderer = screenRenderer;
      if(this.isEnabled==true)
        this.screenRenderer.getRenderer().addActor(this.actor);


      const dataArray = source.getPointData().getScalars() || source.getPointData().getArrays()[0];
      this.widget.setDataArray(dataArray.getData());
    }

    this.widget.applyOpacity(this.piecewiseFunction);
    this.refreshValues()

  }



  updateUseShadow(useShadow:boolean) {

    this.actor.getProperty().setShade(useShadow);

    if(!this.screenRenderer)
      return;

    let renderWindow = this.screenRenderer.getRenderWindow()
    renderWindow.render();

  }


  updateColorMapPreset(color:string) {

  
    let dataRange = this.widget.getOpacityRange();

    let preset = vtkColorMaps.getPresetByName(color);

    let lookupTable = this.actor.getProperty().getRGBTransferFunction(0);
    lookupTable.applyColorMap(preset);
    lookupTable.setMappingRange(...dataRange);
    lookupTable.updateRange();

    if(!this.screenRenderer)
      return;

    let renderWindow = this.screenRenderer.getRenderWindow();
    renderWindow.render();
  }

  updateSpacing(changeValue:number) {


    let space = Math.floor(changeValue);

    if(!this.screenRenderer)
      return;

    let renderWindow = this.screenRenderer.getRenderWindow();

    let sourceDS = this.actor.getMapper().getInputData();
    let sampleDistance =
      0.7 *
      Math.sqrt(
        sourceDS
          .getSpacing()
          .map((v) => v * v)
          .reduce((a, b) => a + b, 0)
      );
    this.actor
      .getMapper()
      .setSampleDistance(sampleDistance * 2 ** (space * 3.0 - 1.5));
    renderWindow.render();
  }


  updateEdgeGradient(changeValue:number) {

    let edgeValue = Math.floor(changeValue);

    let renderWindow = this.screenRenderer ?  this.screenRenderer.getRenderWindow() : null;

    if (edgeValue == 0) {
      this.actor.getProperty().setUseGradientOpacity(0, false);
      if(renderWindow)
        renderWindow.render();
    } else {

      this.actor.getProperty().setUseGradientOpacity(0, true);

      if(!renderWindow)
        return;

      const sourceDS = this.actor.getMapper().getInputData();
      const dataArray =
        sourceDS.getPointData().getScalars() ||
        sourceDS.getPointData().getArrays()[0];
      const dataRange = dataArray.getRange();
      
      const minV = Math.max(0.0, edgeValue - 0.3) / 0.7;
      this.actor
        .getProperty()
        .setGradientOpacityMinimumValue(
          0,
          (dataRange[1] - dataRange[0]) * 0.2 * minV * minV
        );
      this.actor
        .getProperty()
        .setGradientOpacityMaximumValue(
          0,
          (dataRange[1] - dataRange[0]) * 1.0 * edgeValue * edgeValue
        );


        renderWindow.render();
    }

  }
  

}
