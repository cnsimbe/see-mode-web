import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject"
import { filter, take, withLatestFrom } from "rxjs/operators"
import { Observable } from "rxjs/Observable"
import { combineLatest } from 'rxjs/observable/combineLatest';

import vtkBoundingBox from 'vtk.js/Sources/Common/DataModel/BoundingBox';
import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction';
import vtkRenderWindowWithControlBar from 'vtk.js/Sources/Rendering/Misc/RenderWindowWithControlBar';
import vtkGenericRenderWindow from 'vtk.js/Sources/Rendering/Misc/GenericRenderWindow';
import vtkPiecewiseFunction from 'vtk.js/Sources/Common/DataModel/PiecewiseFunction';
import vtkVolume from 'vtk.js/Sources/Rendering/Core/Volume';
import vtkVolumeController from 'vtk.js/Sources/Interaction/UI/VolumeController';
import vtkVolumeMapper from 'vtk.js/Sources/Rendering/Core/VolumeMapper';
import vtkXMLImageDataReader from 'vtk.js/Sources/IO/XML/XMLImageDataReader';
import vtkXMLPolyDataReader from 'vtk.js/Sources/IO/XML/XMLPolyDataReader';

import { VolumeSlicerComponent } from "../volume-slicer/volume-slicer.component"
import { VolumeControlComponent } from "../volume-control/volume-control.component"


@Component({
  selector: 'volume-viewer',
  templateUrl: './volume-viewer.component.html',
  styleUrls: ['./volume-viewer.component.css']
})
export class VolumeViewerComponent implements OnInit {

  screenRenderer= vtkGenericRenderWindow.newInstance();

  sourceData = new BehaviorSubject<any>(null);
  vCtrlSubject = new BehaviorSubject<any>(null);
  sliceCtrlSubject = new BehaviorSubject<any>(null);

  @Input('sliceCtrl') set setSlideCtrl(_sliceCtrl:any){
      this.sliceCtrlSubject.next(_sliceCtrl);
  };


  @Input('vCtrl') set setVCtrl(_vCtrl:any){
      this.vCtrlSubject.next(_vCtrl);
  };

  @ViewChild('container') container:ElementRef; 

  @Input('file') set setFile(_file : File) {

    
    if(!_file)
      return;
  	let fileReader = new FileReader();

  	fileReader.onload = ev=>{

        let vtiReader = vtkXMLImageDataReader.newInstance();
        vtiReader.parseAsArrayBuffer(fileReader.result);
        this.sourceData.next(vtiReader.getOutputData(0))
  	}
  	fileReader.readAsArrayBuffer(_file);
  };



  constructor() { }

  ngOnInit() {



  	let containerElm = this.container.nativeElement as HTMLElement;
  	let screenBackGround = [0,0,0];
    
    this.screenRenderer.setBackground(screenBackGround)
    this.screenRenderer.setContainer(containerElm)

    this.screenRenderer.resize()
  	
    let renderer = this.screenRenderer.getRenderer();
  	let renderWindow = this.screenRenderer.getRenderWindow();
  	renderWindow.getInteractor().setDesiredUpdateRate(15);

    renderer.resetCamera();


    /*

  	let lookupTable = vtkColorTransferFunction.newInstance();
  	let piecewiseFunction = vtkPiecewiseFunction.newInstance();


  	// Pipeline handling
	this.actor.setMapper(this.mapper);
	renderer.addActor(this.actor);


	//Configuration
	this.actor.getProperty().setRGBTransferFunction(0, lookupTable);
	this.actor.getProperty().setScalarOpacity(0, piecewiseFunction);
	this.actor.getProperty().setInterpolationTypeToLinear();
	this.actor.getProperty().setGradientOpacityMinimumValue(0, 0);

	this.actor.getProperty().setShade(true);
	this.actor.getProperty().setUseGradientOpacity(0, true);
	  // - generic good default
	this.actor.getProperty().setGradientOpacityMinimumOpacity(0, 0.0);
	this.actor.getProperty().setGradientOpacityMaximumOpacity(0, 1.0);
	this.actor.getProperty().setAmbient(0.2);
	this.actor.getProperty().setDiffuse(0.7);
	this.actor.getProperty().setSpecular(0.3);
	this.actor.getProperty().setSpecularPower(8.0);

  */

  
  let notNullFilter = filter(s=>s!=null)
  
  
  combineLatest(this.sourceData.pipe(notNullFilter),this.vCtrlSubject.pipe(notNullFilter))
  .subscribe((params)=>{

    let source = params[0];
    let vCtrl = params[1] as VolumeControlComponent;

    vCtrl.setDataSource(this.screenRenderer,source)
  })


  combineLatest(this.sourceData.pipe(notNullFilter),this.sliceCtrlSubject.pipe(notNullFilter))
  .subscribe(params=>{

    let source = params[0];
    let slicerCtrl = params[1] as VolumeSlicerComponent;

    slicerCtrl.setDataSource(this.screenRenderer,source)

  })



  this.sourceData.pipe(notNullFilter).subscribe(source=>this.updateSource(source))



  }


  
  updateSource(source){

    /*
    console.log("source:", source);


  	let dataArray = source.getPointData().getScalars() || source.getPointData().getArrays()[0];
  	let dataRange = dataArray.getRange();

  	this.mapper.setInputData(source);

  	let sampleDistance = 0.7 * Math.sqrt(source.getSpacing().map((v) => v * v).reduce((a, b) => a + b, 0));
	  this.mapper.setSampleDistance(sampleDistance);
	  this.actor.getProperty().setScalarOpacityUnitDistance(0,vtkBoundingBox.getDiagonalLength(source.getBounds()) /Math.max(...source.getDimensions()));
	  this.actor.getProperty().setGradientOpacityMaximumValue(0, (dataRange[1] - dataRange[0]) * 0.05)
  
	  */
    this.screenRenderer.getRenderer().resetCamera();
    this.screenRenderer.getRenderWindow().render()
    setTimeout(()=>this.updateSource(source),330);

  }
 

}
