import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms"
import { CommonModule } from '@angular/common';
import {  VolumeViewerComponent } from "./volume-viewer.component"
import { MaterialModule } from "../material/material.module"

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [VolumeViewerComponent],
  exports :  [VolumeViewerComponent]
})
export class VolumeViewerModule { }
