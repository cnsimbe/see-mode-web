import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs"




@Injectable()
export class RestApiService {

  private server = "0.0.0.0:8000"  

  constructor(private httpClient:HttpClient) { }


  segmentFile(file:File,segment:number,imageFormat='vti'):Observable<any> {

  	let formData = new FormData()
  	formData.append('image', file);
  	formData.append('segmentValue',String(segment));
  	formData.append('imageFormat',imageFormat)

  	return this.httpClient.post(`http://${this.server}/segment`,formData,{responseType:'arraybuffer'})
  }


  applySeedToFile(file:File,seed1:Array<any>,seed2:Array<any>,imageFormat='vti'):Observable<any> {

  	let formData = new FormData()
  	formData.append('image', file);
  	formData.append('seed1',JSON.stringify(seed1));
  	formData.append('seed2',JSON.stringify(seed2));
  	formData.append('imageFormat',imageFormat)

  	return this.httpClient.post(`http://${this.server}/seed`,formData,{responseType:'arraybuffer'})
  }

}
