webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = "\n.view-container {\n\twidth: 100%;\n\tposition: relative;\n}\n\n.view-container  > * {\n\twidth: 100%;\n}\n\n.hideViewer {\n\tposition: absolute;\n\tvisibility:hidden;\n\topacity: 0;\n}\n\n.showViewer {\n\tposition: absolute;\n\tvisibility: visible;\n\topacity: 1;\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n\n<div>\n <button mat-raised-button (click)=\"chooseFile()\">Select file</button>\n <input placeholder=\"file name\" matInput readonly=\"true\" [value]=\"file?.name\" >\n</div>\n\n\n<mat-tab-group [selectedIndex]=\"selectedIndex\" (selectedTabChange)=\"selectedIndex=$event.index\">\n  <mat-tab label=\"Image Viewer and Slicer\">\n  </mat-tab>\n  <mat-tab label=\"Image segmentation\">\n  </mat-tab>\n  <mat-tab label=\"Image Seeding\">\n  </mat-tab>\n</mat-tab-group>\n\n\n<div class=\"view-container\">\n\t<viewer-slicer [ngClass]=\"selectedIndex==0 ? 'showViewer' : 'hideViewer'\" [file]=\"file\"></viewer-slicer>\n\n\t<segment-viewer  [ngClass]=\"selectedIndex==1 ? 'showViewer' : 'hideViewer'\" [file]=\"file\"></segment-viewer>\n\n\t<seed-viewer  [ngClass]=\"selectedIndex==2 ? 'showViewer' : 'hideViewer'\" [file]=\"file\"></seed-viewer>\n</div>\n\n\n  \t\n\n\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = /** @class */ (function () {
    function AppComponent(changeRef) {
        this.changeRef = changeRef;
        this.title = 'app';
        this.controlWidth = 300;
        this.fileElement = document.createElement("input");
        this.selectedIndex = 0;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.fileElement.type = "file";
        this.fileElement.multiple = false;
    };
    AppComponent.prototype.chooseFile = function () {
        var _this = this;
        this.fileElement.click();
        this.fileElement.onchange = function () {
            _this.file = _this.fileElement.files[0];
            _this.changeRef.detectChanges();
        };
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectorRef */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__material_material_module__ = __webpack_require__("./src/app/material/material.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_rest_api_service__ = __webpack_require__("./src/app/services/rest-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__viewer_slicer_viewer_slicer_module__ = __webpack_require__("./src/app/viewer-slicer/viewer-slicer.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__segment_viewer_segment_viewer_module__ = __webpack_require__("./src/app/segment-viewer/segment-viewer.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__seed_viewer_seed_viewer_module__ = __webpack_require__("./src/app/seed-viewer/seed-viewer.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_4__material_material_module__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_6__viewer_slicer_viewer_slicer_module__["a" /* ViewerSliderModule */],
                __WEBPACK_IMPORTED_MODULE_7__segment_viewer_segment_viewer_module__["a" /* SegmentViewerModule */],
                __WEBPACK_IMPORTED_MODULE_8__seed_viewer_seed_viewer_module__["a" /* SeedViewerModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClientModule */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__services_rest_api_service__["a" /* RestApiService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/directives/image-seeder/image-seeder.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageSeederDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_combineLatest__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/combineLatest.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_rest_api_service__ = __webpack_require__("./src/app/services/rest-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_vtk_js_Sources_Rendering_Core_Mapper__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/Mapper/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_vtk_js_Sources_Rendering_Core_Actor__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/Actor/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_vtk_js_Sources_IO_XML_XMLPolyDataReader__ = __webpack_require__("./node_modules/vtk.js/Sources/IO/XML/XMLPolyDataReader/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ImageSeederDirective = /** @class */ (function () {
    function ImageSeederDirective(restApi) {
        this.restApi = restApi;
        this.fileObserv = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["BehaviorSubject"](null);
        this.seedObserv = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["BehaviorSubject"](null);
        this.screenObserv = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["BehaviorSubject"](null);
        this.actors = [];
    }
    Object.defineProperty(ImageSeederDirective.prototype, "outScreen", {
        set: function (screen) {
            this.screenObserv.next(screen);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ImageSeederDirective.prototype, "file", {
        set: function (file) {
            this.fileObserv.next(file);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ImageSeederDirective.prototype, "seedValues", {
        set: function (seedValues) {
            this.seedObserv.next(seedValues);
        },
        enumerable: true,
        configurable: true
    });
    ImageSeederDirective.prototype.ngOnInit = function () {
        var _this = this;
        var validSeedFilter = Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["a" /* filter */])(function (t) { return _this.seedValuesValid(t); });
        var seedValueMapper = Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["b" /* map */])(function (t) { return JSON.parse(JSON.stringify(t)); });
        var noNull = Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["a" /* filter */])(function (t) { return t != null; });
        this.seedObserv.subscribe(function (seedValues) {
            //remove previous actors if not valid seed data
            if (_this.screenRenderer && !_this.seedValuesValid(seedValues)) {
                _this.actors.forEach(function (actor) { return _this.screenRenderer.getRenderer().removeActor(actor); });
                _this.screenRenderer.getRenderWindow().render();
            }
        });
        //debounce to settle on final values as changes occur
        this.seedObserv.pipe(validSeedFilter, seedValueMapper)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["c" /* withLatestFrom */])(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_combineLatest__["a" /* combineLatest */])(this.fileObserv.pipe(noNull), this.screenObserv.pipe(noNull))))
            .map(function (_a) {
            var seedValues = _a[0], params = _a[1];
            params.push(seedValues);
            return params;
        })
            .subscribe(function (_a) {
            var file = _a[0], screen = _a[1], seedValues = _a[2];
            _this.restApi.applySeedToFile(file, seedValues[0], seedValues[1])
                .subscribe(function (binaryFile) {
                var objReader = __WEBPACK_IMPORTED_MODULE_7_vtk_js_Sources_IO_XML_XMLPolyDataReader__["a" /* default */].newInstance();
                objReader.parseAsArrayBuffer(binaryFile);
                //remove previous actors
                if (_this.screenRenderer)
                    _this.actors.forEach(function (actor) { return _this.screenRenderer.getRenderer().removeActor(actor); });
                _this.screenRenderer = screen;
                _this.actors = [];
                var nbOutputs = objReader.getNumberOfOutputPorts();
                for (var idx = 0; idx < nbOutputs; idx++) {
                    var source = objReader.getOutputData(idx);
                    var mapper = __WEBPACK_IMPORTED_MODULE_5_vtk_js_Sources_Rendering_Core_Mapper__["a" /* default */].newInstance();
                    var actor = __WEBPACK_IMPORTED_MODULE_6_vtk_js_Sources_Rendering_Core_Actor__["a" /* default */].newInstance();
                    actor.setMapper(mapper);
                    mapper.setInputData(source);
                    _this.actors.push(actor);
                }
                _this.actors.forEach(function (actor) { return _this.screenRenderer.getRenderer().addActor(actor); });
                _this.screenRenderer.getRenderer().resetCamera();
                _this.screenRenderer.getRenderWindow().render();
            });
        });
    };
    ImageSeederDirective.prototype.seedValuesValid = function (values) {
        return values && values[0] && values[1];
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('outScreen'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], ImageSeederDirective.prototype, "outScreen", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('file'),
        __metadata("design:type", File),
        __metadata("design:paramtypes", [File])
    ], ImageSeederDirective.prototype, "file", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('seedValues'),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], ImageSeederDirective.prototype, "seedValues", null);
    ImageSeederDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: 'image-seeder',
            exportAs: 'ImageSeeder'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__services_rest_api_service__["a" /* RestApiService */]])
    ], ImageSeederDirective);
    return ImageSeederDirective;
}());



/***/ }),

/***/ "./src/app/directives/image-seeder/image-seeder.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageSeederModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__image_seeder_directive__ = __webpack_require__("./src/app/directives/image-seeder/image-seeder.directive.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ImageSeederModule = /** @class */ (function () {
    function ImageSeederModule() {
    }
    ImageSeederModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* CommonModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__image_seeder_directive__["a" /* ImageSeederDirective */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__image_seeder_directive__["a" /* ImageSeederDirective */]]
        })
    ], ImageSeederModule);
    return ImageSeederModule;
}());



/***/ }),

/***/ "./src/app/mapper/ImageMapper.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export extend */
/* unused harmony export newInstance */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vtk_js_Sources_Rendering_Core_ImageMapper_Constants__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/ImageMapper/Constants.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_macro__ = __webpack_require__("./node_modules/vtk.js/Sources/macro.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vtk_js_Sources_Rendering_Core_AbstractMapper__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/AbstractMapper/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Common_Core_Math__ = __webpack_require__("./node_modules/vtk.js/Sources/Common/Core/Math/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vtk_js_Sources_Common_DataModel_Plane__ = __webpack_require__("./node_modules/vtk.js/Sources/Common/DataModel/Plane/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_gl_matrix__ = __webpack_require__("./node_modules/gl-matrix/src/gl-matrix.js");






var vtkWarningMacro = __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_macro__["default"].vtkWarningMacro;
var SlicingMode = __WEBPACK_IMPORTED_MODULE_0_vtk_js_Sources_Rendering_Core_ImageMapper_Constants__["a" /* default */].SlicingMode;
// ----------------------------------------------------------------------------
// vtkImageMapper methods
// ----------------------------------------------------------------------------
function vtkImageMapper(publicAPI, model) {
    // Set our className
    model.classHierarchy.push('vtkImageMapper');
    publicAPI.getSliceAtPosition = function (pos) {
        var image = publicAPI.getInputData();
        var pos3;
        if (pos.length === 3) {
            pos3 = pos;
        }
        else if (Number.isFinite(pos)) {
            var bds = image.getBounds();
            switch (model.slicingMode) {
                case SlicingMode.X:
                    pos3 = [pos, (bds[3] + bds[2]) / 2, (bds[5] + bds[4]) / 2];
                    break;
                case SlicingMode.Y:
                    pos3 = [(bds[1] + bds[0]) / 2, pos, (bds[5] + bds[4]) / 2];
                    break;
                case SlicingMode.Z:
                    pos3 = [(bds[1] + bds[0]) / 2, (bds[3] + bds[2]) / 2, pos];
                    break;
                default:
                    break;
            }
        }
        var ijk = [0, 0, 0];
        image.worldToIndex(pos3, ijk);
        var ex = image.getExtent();
        var ijkMode = publicAPI.getClosestIJKAxis().ijkMode;
        var slice = 0;
        switch (ijkMode) {
            case SlicingMode.I:
                slice = __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Common_Core_Math__["a" /* default */].clampValue(ijk[0], ex[0], ex[1]);
                slice = Math.round(slice);
                break;
            case SlicingMode.J:
                slice = __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Common_Core_Math__["a" /* default */].clampValue(ijk[1], ex[2], ex[3]);
                slice = Math.round(slice);
                break;
            case SlicingMode.K:
                slice = __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Common_Core_Math__["a" /* default */].clampValue(ijk[2], ex[4], ex[5]);
                slice = Math.round(slice);
                break;
            default:
                return 0;
        }
        return slice;
    };
    publicAPI.setSliceFromCamera = function (cam) {
        var fp = cam.getFocalPoint();
        switch (model.slicingMode) {
            case SlicingMode.I:
            case SlicingMode.J:
            case SlicingMode.K:
                {
                    var slice = publicAPI.getSliceAtPosition(fp);
                    publicAPI.setSlice(slice);
                }
                break;
            case SlicingMode.X:
                publicAPI.setSlice(fp[0]);
                break;
            case SlicingMode.Y:
                publicAPI.setSlice(fp[1]);
                break;
            case SlicingMode.Z:
                publicAPI.setSlice(fp[2]);
                break;
            default:
                break;
        }
    };
    publicAPI.setXSlice = function (id) {
        publicAPI.setSlicingMode(SlicingMode.X);
        publicAPI.setSlice(id);
    };
    publicAPI.setYSlice = function (id) {
        publicAPI.setSlicingMode(SlicingMode.Y);
        publicAPI.setSlice(id);
    };
    publicAPI.setZSlice = function (id) {
        publicAPI.setSlicingMode(SlicingMode.Z);
        publicAPI.setSlice(id);
    };
    publicAPI.setISlice = function (id) {
        publicAPI.setSlicingMode(SlicingMode.I);
        publicAPI.setSlice(id);
    };
    publicAPI.setJSlice = function (id) {
        publicAPI.setSlicingMode(SlicingMode.J);
        publicAPI.setSlice(id);
    };
    publicAPI.setKSlice = function (id) {
        publicAPI.setSlicingMode(SlicingMode.K);
        publicAPI.setSlice(id);
    };
    publicAPI.getSlicingModeNormal = function () {
        var out = [0, 0, 0];
        var a = publicAPI.getInputData().getDirection();
        var mat3 = [[a[0], a[1], a[2]], [a[3], a[4], a[5]], [a[6], a[7], a[8]]];
        switch (model.slicingMode) {
            case SlicingMode.X:
                out[0] = 1;
                break;
            case SlicingMode.Y:
                out[1] = 1;
                break;
            case SlicingMode.Z:
                out[2] = 1;
                break;
            case SlicingMode.I:
                __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Common_Core_Math__["a" /* default */].multiply3x3_vect3(mat3, [1, 0, 0], out);
                break;
            case SlicingMode.J:
                __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Common_Core_Math__["a" /* default */].multiply3x3_vect3(mat3, [0, 1, 0], out);
                break;
            case SlicingMode.K:
                __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Common_Core_Math__["a" /* default */].multiply3x3_vect3(mat3, [0, 0, 1], out);
                break;
            default:
                break;
        }
        return out;
    };
    function computeClosestIJKAxis() {
        var inVec3;
        switch (model.slicingMode) {
            case SlicingMode.X:
                inVec3 = [1, 0, 0];
                break;
            case SlicingMode.Y:
                inVec3 = [0, 1, 0];
                break;
            case SlicingMode.Z:
                inVec3 = [0, 0, 1];
                break;
            default:
                model.closestIJKAxis = {
                    ijkMode: model.slicingMode,
                    flip: false,
                };
                return;
        }
        // Project vec3 onto direction cosines
        var out = [0, 0, 0];
        // The direction matrix in vtkImageData is the indexToWorld rotation matrix
        // with a column-major data layout since it is stored as a WebGL matrix.
        // We need the worldToIndex rotation matrix for the projection, and it needs
        // to be in a row-major data layout to use vtkMath for operations.
        // To go from the indexToWorld column-major matrix to the worldToIndex
        // row-major matrix, we need to transpose it (column -> row) then inverse it.
        // However, that 3x3 matrix is a rotation matrix which is orthonormal, meaning
        // that its inverse is equal to its transpose. We therefore need to apply two
        // transpositions resulting in a no-op.
        var a = publicAPI.getInputData().getDirection();
        var mat3 = [[a[0], a[1], a[2]], [a[3], a[4], a[5]], [a[6], a[7], a[8]]];
        __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Common_Core_Math__["a" /* default */].multiply3x3_vect3(mat3, inVec3, out);
        var maxAbs = 0.0;
        var ijkMode = -1;
        var flip = false;
        for (var axis = 0; axis < out.length; ++axis) {
            var absValue = Math.abs(out[axis]);
            if (absValue > maxAbs) {
                maxAbs = absValue;
                flip = out[axis] < 0.0;
                ijkMode = axis;
            }
        }
        if (maxAbs !== 1.0) {
            var xyzLabel = 'IJKXYZ'[model.slicingMode];
            var ijkLabel = 'IJKXYZ'[ijkMode];
            vtkWarningMacro("Unaccurate slicing along " + xyzLabel + " axis which " +
                "is not aligned with any IJK axis of the image data. " +
                ("Using " + ijkLabel + " axis  as a fallback (" + maxAbs + "% aligned). ") +
                "Necessitates slice reformat that is not yet implemented.  " +
                "You can switch the slicing mode on your mapper to do IJK slicing instead.");
        }
        model.closestIJKAxis = { ijkMode: ijkMode, flip: flip };
    }
    publicAPI.setSlicingMode = function (mode) {
        if (model.slicingMode === mode) {
            return;
        }
        model.slicingMode = mode;
        if (publicAPI.getInputData()) {
            computeClosestIJKAxis();
        }
        publicAPI.modified();
    };
    publicAPI.getClosestIJKAxis = function () {
        if ((model.closestIJKAxis === undefined ||
            model.closestIJKAxis.ijkMode === SlicingMode.NONE) &&
            publicAPI.getInputData()) {
            computeClosestIJKAxis();
        }
        return model.closestIJKAxis;
    };
    publicAPI.getBounds = function () {
        var image = publicAPI.getInputData();
        if (!image) {
            return __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Common_Core_Math__["a" /* default */].createUninitializedBounds();
        }
        if (!model.useCustomExtents) {
            return image.getBounds();
        }
        var ex = model.customDisplayExtent.slice();
        var ijkMode = publicAPI.getClosestIJKAxis().ijkMode;
        var nSlice = model.slice;
        if (ijkMode !== model.slicingMode) {
            // If not IJK slicing, get the IJK slice from the XYZ position/slice
            nSlice = publicAPI.getSliceAtPosition(model.slice);
        }
        switch (ijkMode) {
            case SlicingMode.I:
                ex[0] = nSlice;
                ex[1] = nSlice;
                break;
            case SlicingMode.J:
                ex[2] = nSlice;
                ex[3] = nSlice;
                break;
            case SlicingMode.K:
                ex[4] = nSlice;
                ex[5] = nSlice;
                break;
            default:
                break;
        }
        return image.extentToBounds(ex);
    };
    publicAPI.getBoundsForSlice = function (slice, thickness) {
        if (slice === void 0) { slice = model.slice; }
        if (thickness === void 0) { thickness = 0; }
        var image = publicAPI.getInputData();
        if (!image) {
            return __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Common_Core_Math__["a" /* default */].createUninitializedBounds();
        }
        var extent = image.getExtent();
        var ijkMode = publicAPI.getClosestIJKAxis().ijkMode;
        var nSlice = slice;
        if (ijkMode !== model.slicingMode) {
            // If not IJK slicing, get the IJK slice from the XYZ position/slice
            nSlice = publicAPI.getSliceAtPosition(slice);
        }
        switch (ijkMode) {
            case SlicingMode.I:
                extent[0] = nSlice - thickness;
                extent[1] = nSlice + thickness;
                break;
            case SlicingMode.J:
                extent[2] = nSlice - thickness;
                extent[3] = nSlice + thickness;
                break;
            case SlicingMode.K:
                extent[4] = nSlice - thickness;
                extent[5] = nSlice + thickness;
                break;
            default:
                break;
        }
        return image.extentToBounds(extent);
    };
    publicAPI.getIsOpaque = function () { return true; };
    function doPicking(p1, p2) {
        var imageData = publicAPI.getInputData();
        var extent = imageData.getExtent();
        // Slice origin
        var ijk = [extent[0], extent[2], extent[4]];
        var ijkMode = publicAPI.getClosestIJKAxis().ijkMode;
        var nSlice = model.slice;
        if (ijkMode !== model.slicingMode) {
            // If not IJK slicing, get the IJK slice from the XYZ position/slice
            nSlice = publicAPI.getSliceAtPosition(nSlice);
        }
        nSlice -= extent[ijkMode * 2];
        ijk[ijkMode] += nSlice;
        var worldOrigin = [0, 0, 0];
        imageData.indexToWorld(ijk, worldOrigin);
        // Normal computation
        ijk[ijkMode] += 1;
        var worldNormal = [0, 0, 0];
        imageData.indexToWorld(ijk, worldNormal);
        worldNormal[0] -= worldOrigin[0];
        worldNormal[1] -= worldOrigin[1];
        worldNormal[2] -= worldOrigin[2];
        __WEBPACK_IMPORTED_MODULE_5_gl_matrix__["d" /* vec3 */].normalize(worldNormal, worldNormal);
        var intersect = __WEBPACK_IMPORTED_MODULE_4_vtk_js_Sources_Common_DataModel_Plane__["a" /* default */].intersectWithLine(p1, p2, worldOrigin, worldNormal);
        if (intersect.intersection) {
            var point = intersect.x;
            var absoluteIJK = [0, 0, 0];
            imageData.worldToIndex(point, absoluteIJK);
            // `t` is the parametric position along the line
            // defined in Plane.intersectWithLine
            return {
                t: intersect.t,
                absoluteIJK: absoluteIJK,
            };
        }
        return null;
    }
    publicAPI.intersectWithLineForPointPicking = function (p1, p2) {
        var pickingData = doPicking(p1, p2);
        if (pickingData) {
            var imageData = publicAPI.getInputData();
            var extent = imageData.getExtent();
            // Get closer integer ijk
            // NB: point picking means closest slice, means rounding
            var ijk = [
                Math.round(pickingData.absoluteIJK[0]),
                Math.round(pickingData.absoluteIJK[1]),
                Math.round(pickingData.absoluteIJK[2]),
            ];
            // Are we outside our actual extent
            if (ijk[0] < extent[0] ||
                ijk[0] > extent[1] ||
                ijk[1] < extent[2] ||
                ijk[1] > extent[3] ||
                ijk[2] < extent[4] ||
                ijk[2] > extent[5]) {
                return null;
            }
            return {
                t: pickingData.t,
                ijk: ijk,
            };
        }
        return null;
    };
    publicAPI.intersectWithLineForCellPicking = function (p1, p2) {
        var pickingData = doPicking(p1, p2);
        if (pickingData) {
            var imageData = publicAPI.getInputData();
            var extent = imageData.getExtent();
            var absIJK = pickingData.absoluteIJK;
            // Get closer integer ijk
            // NB: cell picking means closest voxel, means flooring
            var ijk = [
                Math.floor(absIJK[0]),
                Math.floor(absIJK[1]),
                Math.floor(absIJK[2]),
            ];
            // Are we outside our actual extent
            if (ijk[0] < extent[0] ||
                ijk[0] > extent[1] - 1 ||
                ijk[1] < extent[2] ||
                ijk[1] > extent[3] - 1 ||
                ijk[2] < extent[4] ||
                ijk[2] > extent[5] - 1) {
                return null;
            }
            // Parametric coordinates within cell
            var pCoords = [
                absIJK[0] - ijk[0],
                absIJK[1] - ijk[1],
                absIJK[2] - ijk[2],
            ];
            return {
                t: pickingData.t,
                ijk: ijk,
                pCoords: pCoords,
            };
        }
        return null;
    };
}
// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------
var DEFAULT_VALUES = {
    displayExtent: [0, 0, 0, 0, 0, 0],
    customDisplayExtent: [0, 0, 0, 0],
    useCustomExtents: false,
    slice: 0,
    slicingMode: SlicingMode.NONE,
    closestIJKAxis: { ijkMode: SlicingMode.NONE, flip: false },
    renderToRectangle: false,
    sliceAtFocalPoint: false,
};
// ----------------------------------------------------------------------------
function extend(publicAPI, model, initialValues) {
    if (initialValues === void 0) { initialValues = {}; }
    Object.assign(model, DEFAULT_VALUES, initialValues);
    // Build VTK API
    __WEBPACK_IMPORTED_MODULE_2_vtk_js_Sources_Rendering_Core_AbstractMapper__["a" /* default */].extend(publicAPI, model, initialValues);
    __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_macro__["default"].get(publicAPI, model, ['slicingMode']);
    __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_macro__["default"].setGet(publicAPI, model, [
        'slice',
        'closestIJKAxis',
        'useCustomExtents',
        'renderToRectangle',
        'sliceAtFocalPoint',
    ]);
    __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_macro__["default"].setGetArray(publicAPI, model, ['customDisplayExtent'], 4);
    // Object methods
    vtkImageMapper(publicAPI, model);
}
// ----------------------------------------------------------------------------
var newInstance = __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_macro__["default"].newInstance(extend, 'vtkImageMapper');
// ----------------------------------------------------------------------------
/* harmony default export */ __webpack_exports__["a"] = (Object.assign({ newInstance: newInstance, extend: extend }, __WEBPACK_IMPORTED_MODULE_0_vtk_js_Sources_Rendering_Core_ImageMapper_Constants__["a" /* default */]));


/***/ }),

/***/ "./src/app/material/material.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaterialModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["d" /* MatFormFieldModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["j" /* MatSliderModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["g" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["h" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["k" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["e" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MatButtonToggleModule */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["d" /* MatFormFieldModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatInputModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MatCheckboxModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["j" /* MatSliderModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["g" /* MatListModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["h" /* MatSelectModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["k" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["e" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MatButtonToggleModule */]
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/seed-control/seed-control.component.css":
/***/ (function(module, exports) {

module.exports = "\n\n.point-container button {\n\tmargin-left: auto;\n}\n\n\n.header {\n\tpadding: 0.5em 0.75em;\n\tbackground-color:grey;\n\tcolor:white;\n\twidth: 100%;\n}"

/***/ }),

/***/ "./src/app/seed-control/seed-control.component.html":
/***/ (function(module, exports) {

module.exports = "\n <mat-list-item>\n\t<div class=\"header\">Selected Points</div>\n </mat-list-item>\n\n <mat-list-item class=\"point-container\">\n \tSeed A =\n \t{{ seedValues[0] ? seedString(seedValues[0]) : 'n/a' }}\n \t<button mat-icon-button color=\"warn\" (click)=\"removeSeed(seedValues[0])\">\n\t\t<mat-icon aria-label=\"remove point A\">delete</mat-icon>\n\t</button>\n </mat-list-item>\n\n\n <mat-list-item class=\"point-container\">\n \tSeed B =\n \t{{ seedValues[1] ? seedString(seedValues[1]) : 'n/a' }}\n \t<button mat-icon-button color=\"warn\" (click)=\"removeSeed(seedValues[1])\">\n\t\t<mat-icon aria-label=\"remove point B\">delete</mat-icon>\n\t</button>\n </mat-list-item>\n\n <!--\n\n  <mat-list-item>\n\t<div class=\"header\">Sphere Point Radius</div>\n </mat-list-item>\n-->\n  <mat-list-item>\n \tPoint Radius\n \t<mat-slider [(ngModel)]=\"pointRadius\" (input)=\"updatePointRadius($event.value)\" #slideCL min=\"0.01\" max=\"1\" step=\"0.01\"></mat-slider>\n </mat-list-item>\n"

/***/ }),

/***/ "./src/app/seed-control/seed-control.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SeedControlComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_Rendering_Core_PointPicker__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/PointPicker/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vtk_js_Sources_Rendering_Core_Mapper__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/Mapper/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Rendering_Core_Actor__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/Actor/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vtk_js_Sources_Filters_Sources_SphereSource__ = __webpack_require__("./node_modules/vtk.js/Sources/Filters/Sources/SphereSource/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_rest_api_service__ = __webpack_require__("./src/app/services/rest-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_observable_combineLatest__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/combineLatest.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SeedControlComponent = /** @class */ (function () {
    function SeedControlComponent(changeRef, restApi) {
        this.changeRef = changeRef;
        this.restApi = restApi;
        this.pointRadius = 0.4;
        this.seedValues = [];
        this.sphereActors = [];
        this.sphereSources = [];
        this.picker = __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_Rendering_Core_PointPicker__["a" /* default */].newInstance();
        this.sliceObserv = new __WEBPACK_IMPORTED_MODULE_6_rxjs__["BehaviorSubject"](null);
        this.inScreenObserv = new __WEBPACK_IMPORTED_MODULE_6_rxjs__["BehaviorSubject"](null);
        this.dataSourceObserv = new __WEBPACK_IMPORTED_MODULE_6_rxjs__["BehaviorSubject"](null);
        this.seedValuesEmitter = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    Object.defineProperty(SeedControlComponent.prototype, "setSlicer", {
        set: function (slicer) {
            this.sliceObserv.next(slicer);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SeedControlComponent.prototype, "setSource", {
        set: function (source) {
            this.dataSourceObserv.next(source);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SeedControlComponent.prototype, "setInScreenRenderer", {
        set: function (screenRender) {
            this.inScreenObserv.next(screenRender);
        },
        enumerable: true,
        configurable: true
    });
    SeedControlComponent.prototype.ngOnInit = function () {
        var _this = this;
        var noNull = Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators__["a" /* filter */])(function (t) { return t != null; });
        Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_observable_combineLatest__["a" /* combineLatest */])(this.dataSourceObserv.pipe(noNull).distinctUntilChanged(), this.inScreenObserv.pipe(noNull).distinctUntilChanged(), this.sliceObserv.pipe(noNull).distinctUntilChanged())
            .subscribe(function (_a) {
            var source = _a[0], screenRenderer = _a[1], slicer = _a[2];
            window["sd"] = _this;
            var renderer = screenRenderer.getRenderer();
            var renderWindow = screenRenderer.getRenderWindow();
            var sliceActors = [slicer.imageActorI, slicer.imageActorJ, slicer.imageActorK];
            sliceActors.forEach(function (sliceActor) { return renderer.addActor(sliceActor); });
            _this.picker.setPickFromList(1);
            _this.picker.initializePickList();
            sliceActors.forEach(function (slideActor) { return _this.picker.addPickList(slideActor); });
            if (_this.seedValues[0])
                _this.removeSeed(_this.seedValues[0]);
            if (_this.seedValues[1])
                _this.removeSeed(_this.seedValues[1]);
            if (screenRenderer != _this.inScreen) {
                _this.inScreen = screenRenderer;
                _this.initializeListener(screenRenderer);
            }
        });
    };
    SeedControlComponent.prototype.initializeListener = function (screenRenderer) {
        var _this = this;
        var renderer = screenRenderer.getRenderer();
        var renderWindow = screenRenderer.getRenderWindow();
        renderWindow.getInteractor().onRightButtonPress(function (callData) {
            if (renderer !== callData.pokedRenderer) {
                return;
            }
            var pos = callData.position;
            var point = [pos.x, pos.y, pos.z];
            var result = _this.picker.pick(point, renderer);
            if (_this.picker.getActors().length > 0) {
                var pickedPointId = _this.picker.getPointId();
                /*
                  Get the closest point selected visible to the current from the viewpoint of the camera
                */
                var cameraPosition = renderer.getActiveCamera().getDirectionOfProjection();
                var pickedPoints = _this.picker.getPickedPositions().sort(function (a, b) { return a[2] > b[2] ? -1 : 1; });
                var pickedPoint = cameraPosition[2] <= 0 ? pickedPoints[0] : pickedPoints[pickedPoints.length - 1];
                if (pickedPoint)
                    _this.addSeedValue(pickedPoint);
            }
            renderWindow.render();
        });
    };
    SeedControlComponent.prototype.seedString = function (seedValue) {
        var point = seedValue[0];
        var intensity = seedValue[1];
        return "[" + Math.floor(point[0]) + ", " + Math.floor(point[1]) + ", " + Math.floor(point[2]) + "] Intensity: " + intensity;
    };
    SeedControlComponent.prototype.updatePointRadius = function (radius) {
        var _this = this;
        this.sphereSources.filter(function (source) { return source != null; }).forEach(function (source) { return source.setRadius(radius); });
        this.sphereSources.forEach(function (sphere, index) {
            if (sphere)
                _this.sphereActors[index].getMapper().setInputData(sphere.getOutputData());
        });
        if (this.inScreen) {
            var renderWindow = this.inScreen.getRenderWindow();
            renderWindow.render();
        }
    };
    SeedControlComponent.prototype.removeSeed = function (seedValue) {
        var index = this.seedValues.indexOf(seedValue);
        if (index >= 0) {
            this.seedValues[index] = null;
            var renderer = this.inScreen.getRenderer();
            var renderWindow = this.inScreen.getRenderWindow();
            renderer.removeActor(this.sphereActors[index]);
            this.sphereActors[index] = null;
            this.sphereSources[index] = null;
            this.seedValuesEmitter.emit(this.seedValues);
            renderWindow.render();
        }
    };
    SeedControlComponent.prototype.addSeedValue = function (pickedPoint) {
        if (this.seedValues[0] && this.seedValues[1])
            return false;
        var indexSelect = this.seedValues[0] ? 1 : 0;
        var renderer = this.inScreen.getRenderer();
        var renderWindow = this.inScreen.getRenderWindow();
        var sphere = __WEBPACK_IMPORTED_MODULE_4_vtk_js_Sources_Filters_Sources_SphereSource__["a" /* default */].newInstance();
        sphere.setCenter(pickedPoint);
        sphere.setRadius(this.pointRadius);
        var sphereMapper = __WEBPACK_IMPORTED_MODULE_2_vtk_js_Sources_Rendering_Core_Mapper__["a" /* default */].newInstance();
        sphereMapper.setInputData(sphere.getOutputData());
        var sphereActor = __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Rendering_Core_Actor__["a" /* default */].newInstance();
        sphereActor.setMapper(sphereMapper);
        sphereActor.getProperty().setColor(0.0, 1.0, 0.0);
        renderer.addActor(sphereActor);
        this.sphereActors[indexSelect] = sphereActor;
        this.sphereSources[indexSelect] = sphere;
        var source = this.dataSourceObserv.getValue();
        var extent = source.getExtent();
        var dataArray = source.getPointData().getScalars() || source.getPointData().getArrays()[0];
        var iExtent = (extent[1] - extent[0]) + 1;
        var jExtent = (extent[3] - extent[2]) + 1;
        var kExtent = (extent[5] - extent[4]) + 1;
        var position = [];
        source.worldToIndex(pickedPoint, position);
        position = position
            .map(function (p) { return Math.round(p); })
            .map(function (p, index) { return Math.max(p, extent[index * 2]); })
            .map(function (p, index) { return Math.min(p, extent[index * 2 + 1]); });
        var deltaI = position[0] - extent[0];
        var deltaJ = position[1] - extent[2];
        var deltaK = position[2] - extent[4];
        var index = deltaI + deltaJ * iExtent + deltaK * iExtent * jExtent;
        var seedValue = [position, dataArray.getData()[index]];
        this.seedValues[indexSelect] = seedValue;
        this.seedValuesEmitter.emit(this.seedValues);
        return true;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('slicer'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SeedControlComponent.prototype, "setSlicer", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('source'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SeedControlComponent.prototype, "setSource", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('inScreen'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SeedControlComponent.prototype, "setInScreenRenderer", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["N" /* Output */])('seedValues'),
        __metadata("design:type", Object)
    ], SeedControlComponent.prototype, "seedValuesEmitter", void 0);
    SeedControlComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'seed-control',
            template: __webpack_require__("./src/app/seed-control/seed-control.component.html"),
            styles: [__webpack_require__("./src/app/seed-control/seed-control.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectorRef */], __WEBPACK_IMPORTED_MODULE_5__services_rest_api_service__["a" /* RestApiService */]])
    ], SeedControlComponent);
    return SeedControlComponent;
}());



/***/ }),

/***/ "./src/app/seed-control/seed-control.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SeedControlModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__seed_control_component__ = __webpack_require__("./src/app/seed-control/seed-control.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__material_material_module__ = __webpack_require__("./src/app/material/material.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SeedControlModule = /** @class */ (function () {
    function SeedControlModule() {
    }
    SeedControlModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["a" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__material_material_module__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormsModule */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_3__seed_control_component__["a" /* SeedControlComponent */]],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__seed_control_component__["a" /* SeedControlComponent */]]
        })
    ], SeedControlModule);
    return SeedControlModule;
}());



/***/ }),

/***/ "./src/app/seed-viewer/seed-viewer.component.css":
/***/ (function(module, exports) {

module.exports = ".main-container {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\t-ms-flex-wrap:nowrap;\n\t    flex-wrap:nowrap;\n}\n\n.control-container {\n\tmin-width: 33.333%;\n}\n\n.render-container {\n\tdisplay:-webkit-box;\n\tdisplay:-ms-flexbox;\n\tdisplay:flex;\n\t-webkit-box-flex:1;\n\t    -ms-flex-positive:1;\n\t        flex-grow:1;\n\theight: 600px;\n\tposition: relative;\n\tbackground-color: white;\n\t-ms-flex-pack: distribute;\n\t    justify-content: space-around;\n\n}\n\n.renderview {\n\twidth: 49%;\n\theight: 100%;\n}\n\n.header {\n\tpadding: 0.5em 0.75em;\n\tbackground-color:grey;\n\tcolor:white;\n\twidth: 100%;\n}\n\n.hideViewer {\n\tposition: absolute;\n\tvisibility: hidden;\n}\n\n.showViewer {\n\tposition: absolute;\n\tvisibility: visible;\n}"

/***/ }),

/***/ "./src/app/seed-viewer/seed-viewer.component.html":
/***/ (function(module, exports) {

module.exports = "<div #container1 class=\"main-container\">\n\t<div class=\"control-container\" >\n\n\t\t<seed-control #segCtrl [inScreen]=\"viewer.screenRenderer\" [source]=\"viewer.sourceData.getValue()\" [slicer]=\"sliceCtrl\" (seedValues)=\"imageSeeder.seedValues=$event\" ></seed-control>\n\n\t\t<mat-list-item>\n\t\t\t<div class=\"header\">Multi Slice Controller</div>\n\t\t\t<volume-slicer  #sliceCtrl></volume-slicer>\t\n\t\t</mat-list-item>\n\n\t\t<image-seeder #imageSeeder=\"ImageSeeder\" [outScreen]=\"viewer2.screenRenderer\" [file]=\"file\"></image-seeder>\n\t\n\t\n\n\t</div>\n\n\t<div class=\"render-container\">\n\n\t\t<div class=\"renderview\" >\n\n\t\t\t<volume-viewer #viewer [file]=\"file\" [vCtrl]=\"uiCtrl\" [sliceCtrl]=\"sliceCtrl\"></volume-viewer>\n\t\t</div>\n\t\t<div class=\"renderview\" >\n\n\t\t\t<volume-viewer #viewer2></volume-viewer>\n\t\t</div>\n\n\t\t<!--\n\t\t<mat-tab-group [selectedIndex]=\"selectedIndex\" (selectedTabChange)=\"selectedIndex=$event.index\">\n\t\t  <mat-tab label=\"Point Selection\">\n\t\t  </mat-tab>\n\t\t  <mat-tab label=\"Image Seeded View\">\n\t\t  </mat-tab>\n\t\t</mat-tab-group>\n\n\n\n\t\t<div class=\"renderview\" [ngClass]=\"selectedIndex==0 ? 'showViewer' : 'hideViewer' \">\n\n\t\t\t<volume-viewer #viewer [file]=\"file\" [vCtrl]=\"uiCtrl\" [sliceCtrl]=\"sliceCtrl\"></volume-viewer>\n\t\t</div>\n\t\t<div class=\"renderview\" [ngClass]=\"selectedIndex==1 ? 'showViewer' : 'hideViewer' \">\n\n\t\t\t<volume-viewer #viewer2></volume-viewer>\n\t\t</div>\n\t\t-->\n\t</div>\n\t\n</div>"

/***/ }),

/***/ "./src/app/seed-viewer/seed-viewer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SeedViewerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SeedViewerComponent = /** @class */ (function () {
    function SeedViewerComponent(changeRef) {
        this.changeRef = changeRef;
        this.selectedIndex = 0;
        this.controlWidth = 300;
    }
    SeedViewerComponent.prototype.ngOnDestroy = function () {
        window.removeEventListener('resize', this.resizeListener);
    };
    SeedViewerComponent.prototype.ngOnInit = function () {
        var _this = this;
        var context = this;
        this.resizeListener = function () { return _this.updateControlWidth(context); };
        window.addEventListener('resize', this.resizeListener);
        this.updateControlWidth(this);
    };
    SeedViewerComponent.prototype.updateControlWidth = function (context) {
        context.controlWidth = context.containerRef.nativeElement.clientWidth / 3;
        context.changeRef.detectChanges();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('file'),
        __metadata("design:type", File)
    ], SeedViewerComponent.prototype, "file", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('container1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], SeedViewerComponent.prototype, "containerRef", void 0);
    SeedViewerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'seed-viewer',
            template: __webpack_require__("./src/app/seed-viewer/seed-viewer.component.html"),
            styles: [__webpack_require__("./src/app/seed-viewer/seed-viewer.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectorRef */]])
    ], SeedViewerComponent);
    return SeedViewerComponent;
}());



/***/ }),

/***/ "./src/app/seed-viewer/seed-viewer.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SeedViewerModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__seed_viewer_component__ = __webpack_require__("./src/app/seed-viewer/seed-viewer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__material_material_module__ = __webpack_require__("./src/app/material/material.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__volume_control_volume_control_module__ = __webpack_require__("./src/app/volume-control/volume-control.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__volume_viewer_volume_viewer_module__ = __webpack_require__("./src/app/volume-viewer/volume-viewer.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__seed_control_seed_control_module__ = __webpack_require__("./src/app/seed-control/seed-control.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__volume_slicer_volume_slicer_module__ = __webpack_require__("./src/app/volume-slicer/volume-slicer.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__directives_image_seeder_image_seeder_module__ = __webpack_require__("./src/app/directives/image-seeder/image-seeder.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var SeedViewerModule = /** @class */ (function () {
    function SeedViewerModule() {
    }
    SeedViewerModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["a" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__material_material_module__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_5__volume_control_volume_control_module__["a" /* VolumeControlModule */],
                __WEBPACK_IMPORTED_MODULE_6__volume_viewer_volume_viewer_module__["a" /* VolumeViewerModule */],
                __WEBPACK_IMPORTED_MODULE_7__seed_control_seed_control_module__["a" /* SeedControlModule */],
                __WEBPACK_IMPORTED_MODULE_8__volume_slicer_volume_slicer_module__["a" /* VolumeSlicerModule */],
                __WEBPACK_IMPORTED_MODULE_9__directives_image_seeder_image_seeder_module__["a" /* ImageSeederModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__seed_viewer_component__["a" /* SeedViewerComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_3__seed_viewer_component__["a" /* SeedViewerComponent */]]
        })
    ], SeedViewerModule);
    return SeedViewerModule;
}());



/***/ }),

/***/ "./src/app/segment-control/segment-control.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/segment-control/segment-control.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-list>\n <mat-list-item>\n \tThreshold\n \t<mat-slider [(ngModel)]=\"segmentValue\" (input)=\"segmentValue=$event.value\" (change)=\"updateSegment($event.value)\" #slideSeg min=\"100\" max=\"800\" step=\"1\"></mat-slider>\n\n \t{{segmentValue}}\n\n </mat-list-item>\n\n</mat-list>"

/***/ }),

/***/ "./src/app/segment-control/segment-control.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SegmentControlComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_rest_api_service__ = __webpack_require__("./src/app/services/rest-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vtk_js_Sources_Rendering_Core_Mapper__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/Mapper/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Rendering_Core_Actor__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/Actor/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vtk_js_Sources_IO_XML_XMLPolyDataReader__ = __webpack_require__("./node_modules/vtk.js/Sources/IO/XML/XMLPolyDataReader/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SegmentControlComponent = /** @class */ (function () {
    function SegmentControlComponent(restApi) {
        this.restApi = restApi;
        this.actors = [];
        this.isEnabled = true;
        this.segmentValue = 480;
    }
    Object.defineProperty(SegmentControlComponent.prototype, "setFile", {
        set: function (file) {
            if (!file || this.file == file)
                return;
            this.file = file;
            this.updateSegment(this.segmentValue);
        },
        enumerable: true,
        configurable: true
    });
    SegmentControlComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(SegmentControlComponent.prototype, "setRenderScreen", {
        set: function (renderScreen) {
            var _this = this;
            if (!renderScreen)
                return;
            if (this.screenRenderer != renderScreen) {
                this.screenRenderer = renderScreen;
                if (this.isEnabled == true)
                    this.actors.forEach(function (actor) { return _this.screenRenderer.getRenderer().addActor(actor); });
            }
            this.screenRenderer.getRenderWindow().render();
        },
        enumerable: true,
        configurable: true
    });
    SegmentControlComponent.prototype.enable = function () {
        var _this = this;
        this.isEnabled = true;
        if (!this.screenRenderer)
            return;
        var renderer = this.screenRenderer.getRenderer();
        this.actors.forEach(function (actor) { return _this.screenRenderer.getRenderer().addActor(actor); });
        this.screenRenderer.getRenderWindow().render();
    };
    SegmentControlComponent.prototype.disable = function () {
        var _this = this;
        this.isEnabled = false;
        if (!this.screenRenderer)
            return;
        var renderer = this.screenRenderer.getRenderer();
        this.actors.forEach(function (actor) { return _this.screenRenderer.getRenderer().removeActor(actor); });
        this.screenRenderer.getRenderWindow().render();
    };
    SegmentControlComponent.prototype.updateSegment = function (value) {
        var _this = this;
        this.restApi.segmentFile(this.file, this.segmentValue).subscribe(function (binaryResult) {
            var objReader = __WEBPACK_IMPORTED_MODULE_4_vtk_js_Sources_IO_XML_XMLPolyDataReader__["a" /* default */].newInstance();
            objReader.parseAsArrayBuffer(binaryResult);
            // remove previous actors
            if (_this.screenRenderer)
                _this.actors.forEach(function (actor) { return _this.screenRenderer.getRenderer().removeActor(actor); });
            _this.actors = [];
            var nbOutputs = objReader.getNumberOfOutputPorts();
            for (var idx = 0; idx < nbOutputs; idx++) {
                var source = objReader.getOutputData(idx);
                var mapper = __WEBPACK_IMPORTED_MODULE_2_vtk_js_Sources_Rendering_Core_Mapper__["a" /* default */].newInstance();
                var actor = __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Rendering_Core_Actor__["a" /* default */].newInstance();
                actor.setMapper(mapper);
                mapper.setInputData(source);
                _this.actors.push(actor);
            }
            if (_this.isEnabled == true && _this.screenRenderer)
                _this.actors.forEach(function (actor) { return _this.screenRenderer.getRenderer().addActor(actor); });
            if (_this.screenRenderer) {
                _this.screenRenderer.getRenderer().resetCamera();
                _this.screenRenderer.getRenderWindow().render();
                setTimeout(function () {
                    _this.screenRenderer.getRenderer().resetCamera();
                    _this.screenRenderer.getRenderWindow().render();
                }, 330);
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('file'),
        __metadata("design:type", File),
        __metadata("design:paramtypes", [File])
    ], SegmentControlComponent.prototype, "setFile", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('screenRenderer'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SegmentControlComponent.prototype, "setRenderScreen", null);
    SegmentControlComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'segment-control',
            template: __webpack_require__("./src/app/segment-control/segment-control.component.html"),
            styles: [__webpack_require__("./src/app/segment-control/segment-control.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_rest_api_service__["a" /* RestApiService */]])
    ], SegmentControlComponent);
    return SegmentControlComponent;
}());



/***/ }),

/***/ "./src/app/segment-control/segment-control.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SegmentControlModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__segment_control_component__ = __webpack_require__("./src/app/segment-control/segment-control.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__material_material_module__ = __webpack_require__("./src/app/material/material.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SegmentControlModule = /** @class */ (function () {
    function SegmentControlModule() {
    }
    SegmentControlModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["a" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__material_material_module__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormsModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__segment_control_component__["a" /* SegmentControlComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_3__segment_control_component__["a" /* SegmentControlComponent */]]
        })
    ], SegmentControlModule);
    return SegmentControlModule;
}());



/***/ }),

/***/ "./src/app/segment-viewer/segment-viewer.component.css":
/***/ (function(module, exports) {

module.exports = ".main-container {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\t-ms-flex-wrap:nowrap;\n\t    flex-wrap:nowrap;\n}\n\n.control-container {\n\tmin-width: 33.333%;\n}\n\n.render-container {\n\t-webkit-box-flex:1;\n\t    -ms-flex-positive:1;\n\t        flex-grow:1;\n\theight: 600px;\n\n}\n\n.renderview {\n\theight: 100%;\n}"

/***/ }),

/***/ "./src/app/segment-viewer/segment-viewer.component.html":
/***/ (function(module, exports) {

module.exports = "<div #container1 class=\"main-container\">\n\t<div class=\"control-container\" >\n\n\t\t<segment-control #segCtrl [file]=\"file\" [screenRenderer]=\"viewer.screenRenderer\" ></segment-control>\n\t</div>\n\n\n\t<div class=\"render-container\">\n\t\t<div class=\"renderview\">\n\t\t\t<volume-viewer #viewer ></volume-viewer>\n\t\t</div>\n\t</div>\n\t\n</div>"

/***/ }),

/***/ "./src/app/segment-viewer/segment-viewer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SegmentViewerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SegmentViewerComponent = /** @class */ (function () {
    function SegmentViewerComponent() {
    }
    SegmentViewerComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('file'),
        __metadata("design:type", File)
    ], SegmentViewerComponent.prototype, "file", void 0);
    SegmentViewerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'segment-viewer',
            template: __webpack_require__("./src/app/segment-viewer/segment-viewer.component.html"),
            styles: [__webpack_require__("./src/app/segment-viewer/segment-viewer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SegmentViewerComponent);
    return SegmentViewerComponent;
}());



/***/ }),

/***/ "./src/app/segment-viewer/segment-viewer.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SegmentViewerModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__segment_viewer_component__ = __webpack_require__("./src/app/segment-viewer/segment-viewer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__material_material_module__ = __webpack_require__("./src/app/material/material.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__volume_viewer_volume_viewer_module__ = __webpack_require__("./src/app/volume-viewer/volume-viewer.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__segment_control_segment_control_module__ = __webpack_require__("./src/app/segment-control/segment-control.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var SegmentViewerModule = /** @class */ (function () {
    function SegmentViewerModule() {
    }
    SegmentViewerModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["a" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_6__segment_control_segment_control_module__["a" /* SegmentControlModule */],
                __WEBPACK_IMPORTED_MODULE_5__volume_viewer_volume_viewer_module__["a" /* VolumeViewerModule */],
                __WEBPACK_IMPORTED_MODULE_4__material_material_module__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormsModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__segment_viewer_component__["a" /* SegmentViewerComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_3__segment_viewer_component__["a" /* SegmentViewerComponent */]]
        })
    ], SegmentViewerModule);
    return SegmentViewerModule;
}());



/***/ }),

/***/ "./src/app/services/rest-api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RestApiService = /** @class */ (function () {
    function RestApiService(httpClient) {
        this.httpClient = httpClient;
        this.server = "0.0.0.0:8000";
    }
    RestApiService.prototype.segmentFile = function (file, segment, imageFormat) {
        if (imageFormat === void 0) { imageFormat = 'vti'; }
        var formData = new FormData();
        formData.append('image', file);
        formData.append('segmentValue', String(segment));
        formData.append('imageFormat', imageFormat);
        return this.httpClient.post("http://" + this.server + "/segment", formData, { responseType: 'arraybuffer' });
    };
    RestApiService.prototype.applySeedToFile = function (file, seed1, seed2, imageFormat) {
        if (imageFormat === void 0) { imageFormat = 'vti'; }
        var formData = new FormData();
        formData.append('image', file);
        formData.append('seed1', JSON.stringify(seed1));
        formData.append('seed2', JSON.stringify(seed2));
        formData.append('imageFormat', imageFormat);
        return this.httpClient.post("http://" + this.server + "/seed", formData, { responseType: 'arraybuffer' });
    };
    RestApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], RestApiService);
    return RestApiService;
}());



/***/ }),

/***/ "./src/app/viewer-slicer/viewer-slicer.component.css":
/***/ (function(module, exports) {

module.exports = ".main-container {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\t-ms-flex-wrap:nowrap;\n\t    flex-wrap:nowrap;\n}\n\n.control-container {\n\tmin-width: 33.333%;\n}\n\n.render-container {\n\t-webkit-box-flex:1;\n\t    -ms-flex-positive:1;\n\t        flex-grow:1;\n\theight: 600px;\n\n}\n\n.renderview {\n\theight: 100%;\n}\n\n.container1 {\n\tdisplay: -webkit-box;\n\tdisplay: -ms-flexbox;\n\tdisplay: flex;\n\twidth: 100%;\n\t-webkit-box-pack: justify;\n\t    -ms-flex-pack: justify;\n\t        justify-content: space-between;\n}\n\n.container1 > * { \n\tpadding: 1.5em 0em;\n\tpadding-right: 1em;\n }\n\n.header {\n\tpadding: 0.5em 0.75em;\n\tbackground-color:grey;\n\tcolor:white;\n}"

/***/ }),

/***/ "./src/app/viewer-slicer/viewer-slicer.component.html":
/***/ (function(module, exports) {

module.exports = "<div #container1 class=\"main-container\">\n\t<div class=\"control-container\" >\n\n\t\t<div class=\"container1\">\n\n\t\t\t <mat-checkbox [checked]=\"true\" (change)=\"$event.checked==true ? uiCtrl.enable() : uiCtrl.disable()\">Enable Volume Viewer</mat-checkbox>\n\t\t\t \n\t\t\t <mat-checkbox [checked]=\"true\" (change)=\"$event.checked==true ? sliceCtrl.enable() : sliceCtrl.disable()\">Enable Slicing</mat-checkbox>\n\n\t\t</div>\n\n\t\t<mat-list-item>\n\t\t\t<div class=\"header\">UI View Controller</div>\n\t\t\t<volume-control [height]=\"200\" [width]=\"controlWidth\" #uiCtrl></volume-control>\n\t\t</mat-list-item>\n\n\t\t<mat-list-item>\n\t\t\t<div class=\"header\">Multi Slice Controller</div>\n\t\t\t<volume-slicer  #sliceCtrl></volume-slicer>\t\n\t\t</mat-list-item>\n\t\t\n\t</div>\n\n\n\t<div class=\"render-container\">\n\t\t<div class=\"renderview\">\n\t\t\t<volume-viewer  [file]=\"file\" [vCtrl]=\"uiCtrl\" [sliceCtrl]=\"sliceCtrl\"></volume-viewer>\n\t\t</div>\n\t</div>\n\t\n</div>"

/***/ }),

/***/ "./src/app/viewer-slicer/viewer-slicer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewerSlicerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ViewerSlicerComponent = /** @class */ (function () {
    function ViewerSlicerComponent(changeRef) {
        this.changeRef = changeRef;
        this.controlWidth = 300;
    }
    ViewerSlicerComponent.prototype.ngOnDestroy = function () {
        window.removeEventListener('resize', this.resizeListener);
    };
    ViewerSlicerComponent.prototype.ngOnInit = function () {
        var _this = this;
        var context = this;
        this.resizeListener = function () { return _this.updateControlWidth(context); };
        window.addEventListener('resize', this.resizeListener);
        this.updateControlWidth(this);
    };
    ViewerSlicerComponent.prototype.updateControlWidth = function (context) {
        context.controlWidth = context.containerRef.nativeElement.clientWidth / 3;
        context.changeRef.detectChanges();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('file'),
        __metadata("design:type", File)
    ], ViewerSlicerComponent.prototype, "file", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('container1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], ViewerSlicerComponent.prototype, "containerRef", void 0);
    ViewerSlicerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'viewer-slicer',
            template: __webpack_require__("./src/app/viewer-slicer/viewer-slicer.component.html"),
            styles: [__webpack_require__("./src/app/viewer-slicer/viewer-slicer.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["j" /* ChangeDetectorRef */]])
    ], ViewerSlicerComponent);
    return ViewerSlicerComponent;
}());



/***/ }),

/***/ "./src/app/viewer-slicer/viewer-slicer.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewerSliderModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__viewer_slicer_component__ = __webpack_require__("./src/app/viewer-slicer/viewer-slicer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__material_material_module__ = __webpack_require__("./src/app/material/material.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__volume_viewer_volume_viewer_module__ = __webpack_require__("./src/app/volume-viewer/volume-viewer.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__volume_control_volume_control_module__ = __webpack_require__("./src/app/volume-control/volume-control.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__volume_slicer_volume_slicer_module__ = __webpack_require__("./src/app/volume-slicer/volume-slicer.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var ViewerSliderModule = /** @class */ (function () {
    function ViewerSliderModule() {
    }
    ViewerSliderModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["a" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__material_material_module__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_6__volume_control_volume_control_module__["a" /* VolumeControlModule */],
                __WEBPACK_IMPORTED_MODULE_7__volume_slicer_volume_slicer_module__["a" /* VolumeSlicerModule */],
                __WEBPACK_IMPORTED_MODULE_5__volume_viewer_volume_viewer_module__["a" /* VolumeViewerModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__viewer_slicer_component__["a" /* ViewerSlicerComponent */]],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__viewer_slicer_component__["a" /* ViewerSlicerComponent */]
            ]
        })
    ], ViewerSliderModule);
    return ViewerSliderModule;
}());



/***/ }),

/***/ "./src/app/volume-control/volume-control.component.css":
/***/ (function(module, exports) {

module.exports = ".row1 {\n\tdisplay:-webkit-box;\n\tdisplay:-ms-flexbox;\n\tdisplay:flex;\n\t-webkit-box-pack: justify;\n\t    -ms-flex-pack: justify;\n\t        justify-content: space-between;\n\t-webkit-box-align: center;\n\t    -ms-flex-align: center;\n\t        align-items: center;\n}\n\n.row1 > mat-select {\n\tmax-width: 180px;\n}"

/***/ }),

/***/ "./src/app/volume-control/volume-control.component.html":
/***/ (function(module, exports) {

module.exports = "<div container #container>\n\n\t<div class=\"row1\">\n\t\t<mat-checkbox [checked]=\"use_shadow\" (change)=\"updateUseShadow($event.checked)\">Use Shadow</mat-checkbox>\n\n\n\t\t<mat-select placeholder=\"Color map\" (selectionChange)=\"updateColorMapPreset($event.value)\" [(ngModel)]=\"preset_color\" >\n\t\t    <mat-option *ngFor=\"let color of colorOptions\" [value]=\"color\">\n\t\t      {{ color }}\n\t\t    </mat-option>\n\t\t</mat-select>\n\t</div>\n\n\t<div class=\"row1\">\n\t\t<div>\n\t\t\t<span>Spacing</span>\n\t\t\t<mat-slider [(ngModel)]=\"space_value\" min=\"0\" max=\"1\" value=\"0.4\" step=\"0.01\" (input)=\"updateSpacing($event.value)\"></mat-slider>\n\t\t</div>\n\n\n\t\t<div>\n\t\t\t<span>Contract</span>\n\t\t\t<mat-slider [(ngModel)]=\"edge_value\" min=\"0\" max=\"1\" value=\"0.2\" step=\"0.01\" (input)=\"updateEdgeGradient($event.value)\"></mat-slider>\n\t\t</div>\n\n\t</div>\n\t\n\t\n</div>\n"

/***/ }),

/***/ "./src/app/volume-control/volume-control.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VolumeControlComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_Rendering_Core_ColorTransferFunction_ColorMaps__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/ColorTransferFunction/ColorMaps.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vtk_js_Sources_Interaction_Widgets_PiecewiseGaussianWidget__ = __webpack_require__("./node_modules/vtk.js/Sources/Interaction/Widgets/PiecewiseGaussianWidget/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Rendering_Core_Volume__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/Volume/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vtk_js_Sources_Rendering_Core_VolumeMapper__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/VolumeMapper/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_vtk_js_Sources_Common_DataModel_BoundingBox__ = __webpack_require__("./node_modules/vtk.js/Sources/Common/DataModel/BoundingBox/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_vtk_js_Sources_Rendering_Core_ColorTransferFunction__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/ColorTransferFunction/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_vtk_js_Sources_Common_DataModel_PiecewiseFunction__ = __webpack_require__("./node_modules/vtk.js/Sources/Common/DataModel/PiecewiseFunction/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var VolumeControlComponent = /** @class */ (function () {
    function VolumeControlComponent() {
        this.actor = __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Rendering_Core_Volume__["a" /* default */].newInstance();
        this.mapper = __WEBPACK_IMPORTED_MODULE_4_vtk_js_Sources_Rendering_Core_VolumeMapper__["a" /* default */].newInstance();
        this.lookupTable = __WEBPACK_IMPORTED_MODULE_6_vtk_js_Sources_Rendering_Core_ColorTransferFunction__["a" /* default */].newInstance();
        this.piecewiseFunction = __WEBPACK_IMPORTED_MODULE_7_vtk_js_Sources_Common_DataModel_PiecewiseFunction__["a" /* default */].newInstance();
        this.isEnabled = true;
        this.space_value = 0.4;
        this.edge_value = 0.2;
        this.width = 400;
        this.height = 150;
        this.use_shadow = true;
        this.preset_color = 'erdc_rainbow_bright';
        this.colorOptions = __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_Rendering_Core_ColorTransferFunction_ColorMaps__["a" /* default */].rgbPresetNames;
    }
    Object.defineProperty(VolumeControlComponent.prototype, "setWidth", {
        //private vCtrl = vtkVolumeController.newInstance({
        //size: [this.width, this.height],
        //  rescaleColorMap: true,
        //});
        set: function (width) {
            if (width == 0 || width) {
                this.width = width;
                if (this.widget)
                    this.widget.setSize(this.width, this.height);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VolumeControlComponent.prototype, "setHeight", {
        set: function (height) {
            if (height == 0 || height) {
                this.height = height;
                if (this.widget)
                    this.widget.setSize(this.width, this.height);
            }
        },
        enumerable: true,
        configurable: true
    });
    VolumeControlComponent.prototype.ngOnInit = function () {
        var _this = this;
        var containerElem = this.container.nativeElement;
        // Pipeline handling
        this.actor.setMapper(this.mapper);
        //Configuration
        this.actor.getProperty().setRGBTransferFunction(0, this.lookupTable);
        this.actor.getProperty().setScalarOpacity(0, this.piecewiseFunction);
        this.actor.getProperty().setInterpolationTypeToLinear();
        this.actor.getProperty().setGradientOpacityMinimumValue(0, 0);
        this.actor.getProperty().setShade(true);
        this.actor.getProperty().setUseGradientOpacity(0, true);
        // - generic good default
        this.actor.getProperty().setGradientOpacityMinimumOpacity(0, 0.0);
        this.actor.getProperty().setGradientOpacityMaximumOpacity(0, 1.0);
        this.actor.getProperty().setAmbient(0.2);
        this.actor.getProperty().setDiffuse(0.7);
        this.actor.getProperty().setSpecular(0.3);
        this.actor.getProperty().setSpecularPower(8.0);
        this.widget = __WEBPACK_IMPORTED_MODULE_2_vtk_js_Sources_Interaction_Widgets_PiecewiseGaussianWidget__["a" /* default */].newInstance({
            numberOfBins: 256,
            size: [this.width, this.height],
        });
        this.widget.updateStyle({
            backgroundColor: 'rgba(255, 255, 255, 0.6)',
            histogramColor: 'rgba(100, 100, 100, 0.5)',
            strokeColor: 'rgb(0, 0, 0)',
            activeColor: 'rgb(255, 255, 255)',
            handleColor: 'rgb(50, 150, 50)',
            buttonDisableFillColor: 'rgba(255, 255, 255, 0.5)',
            buttonDisableStrokeColor: 'rgba(0, 0, 0, 0.5)',
            buttonStrokeColor: 'rgba(0, 0, 0, 1)',
            buttonFillColor: 'rgba(255, 255, 255, 1)',
            strokeWidth: 2,
            activeStrokeWidth: 3,
            buttonStrokeWidth: 1.5,
            handleWidth: 3,
            iconSize: 0,
            padding: 10,
        });
        this.widget.addGaussian(0.5, 1.0, 0.5, 0.5, 0.4);
        //this.widget.setDataArray(dataArray.getData());
        this.widget.setColorTransferFunction(this.lookupTable);
        this.widget.applyOpacity(this.piecewiseFunction);
        this.widget.setContainer(containerElem);
        this.widget.bindMouseListeners();
        this.widget.onOpacityChange(function () {
            _this.widget.applyOpacity(_this.piecewiseFunction);
            _this.updateColorMapPreset(_this.preset_color);
            if (!_this.screenRenderer)
                return;
            var renderWindow = _this.screenRenderer.getRenderWindow();
            if (!renderWindow.getInteractor().isAnimating()) {
                renderWindow.render();
            }
        });
        this.widget.onAnimation(function (start) {
            if (!_this.screenRenderer)
                return;
            var renderWindow = _this.screenRenderer.getRenderWindow();
            if (start) {
                renderWindow.getInteractor().requestAnimation(_this.widget);
            }
            else {
                renderWindow.getInteractor().cancelAnimation(_this.widget);
                renderWindow.render();
            }
        });
        this.lookupTable.onModified(function () {
            _this.widget.render();
            if (!_this.screenRenderer)
                return;
            var renderWindow = _this.screenRenderer.getRenderWindow();
            if (!renderWindow.getInteractor().isAnimating()) {
                renderWindow.render();
            }
        });
        this.refreshValues();
    };
    VolumeControlComponent.prototype.refreshValues = function () {
        this.updateUseShadow(this.use_shadow);
        this.updateColorMapPreset(this.preset_color);
        this.updateSpacing(this.space_value);
        this.updateEdgeGradient(this.edge_value);
        this.widget.render();
    };
    VolumeControlComponent.prototype.enable = function () {
        this.isEnabled = true;
        if (!this.screenRenderer)
            return;
        var renderer = this.screenRenderer.getRenderer();
        renderer.addActor(this.actor);
        this.screenRenderer.getRenderWindow().render();
    };
    VolumeControlComponent.prototype.disable = function () {
        this.isEnabled = false;
        if (!this.screenRenderer)
            return;
        var renderer = this.screenRenderer.getRenderer();
        renderer.removeActor(this.actor);
        this.screenRenderer.getRenderWindow().render();
    };
    VolumeControlComponent.prototype.setDataSource = function (screenRenderer, source) {
        window["e"] = source;
        var dataArray = source.getPointData().getScalars() || source.getPointData().getArrays()[0];
        var dataRange = dataArray.getRange();
        this.mapper.setInputData(source);
        var sampleDistance = 0.7 * Math.sqrt(source.getSpacing().map(function (v) { return v * v; }).reduce(function (a, b) { return a + b; }, 0));
        this.mapper.setSampleDistance(sampleDistance);
        this.actor.getProperty().setScalarOpacityUnitDistance(0, __WEBPACK_IMPORTED_MODULE_5_vtk_js_Sources_Common_DataModel_BoundingBox__["a" /* default */].getDiagonalLength(source.getBounds()) / Math.max.apply(Math, source.getDimensions()));
        this.actor.getProperty().setGradientOpacityMaximumValue(0, (dataRange[1] - dataRange[0]) * 0.05);
        if (this.screenRenderer != screenRenderer) {
            this.screenRenderer = screenRenderer;
            if (this.isEnabled == true)
                this.screenRenderer.getRenderer().addActor(this.actor);
            var dataArray_1 = source.getPointData().getScalars() || source.getPointData().getArrays()[0];
            this.widget.setDataArray(dataArray_1.getData());
        }
        this.widget.applyOpacity(this.piecewiseFunction);
        this.refreshValues();
    };
    VolumeControlComponent.prototype.updateUseShadow = function (useShadow) {
        this.actor.getProperty().setShade(useShadow);
        if (!this.screenRenderer)
            return;
        var renderWindow = this.screenRenderer.getRenderWindow();
        renderWindow.render();
    };
    VolumeControlComponent.prototype.updateColorMapPreset = function (color) {
        var dataRange = this.widget.getOpacityRange();
        var preset = __WEBPACK_IMPORTED_MODULE_1_vtk_js_Sources_Rendering_Core_ColorTransferFunction_ColorMaps__["a" /* default */].getPresetByName(color);
        var lookupTable = this.actor.getProperty().getRGBTransferFunction(0);
        lookupTable.applyColorMap(preset);
        lookupTable.setMappingRange.apply(lookupTable, dataRange);
        lookupTable.updateRange();
        if (!this.screenRenderer)
            return;
        var renderWindow = this.screenRenderer.getRenderWindow();
        renderWindow.render();
    };
    VolumeControlComponent.prototype.updateSpacing = function (changeValue) {
        var space = Math.floor(changeValue);
        if (!this.screenRenderer)
            return;
        var renderWindow = this.screenRenderer.getRenderWindow();
        var sourceDS = this.actor.getMapper().getInputData();
        var sampleDistance = 0.7 *
            Math.sqrt(sourceDS
                .getSpacing()
                .map(function (v) { return v * v; })
                .reduce(function (a, b) { return a + b; }, 0));
        this.actor
            .getMapper()
            .setSampleDistance(sampleDistance * Math.pow(2, (space * 3.0 - 1.5)));
        renderWindow.render();
    };
    VolumeControlComponent.prototype.updateEdgeGradient = function (changeValue) {
        var edgeValue = Math.floor(changeValue);
        var renderWindow = this.screenRenderer ? this.screenRenderer.getRenderWindow() : null;
        if (edgeValue == 0) {
            this.actor.getProperty().setUseGradientOpacity(0, false);
            if (renderWindow)
                renderWindow.render();
        }
        else {
            this.actor.getProperty().setUseGradientOpacity(0, true);
            if (!renderWindow)
                return;
            var sourceDS = this.actor.getMapper().getInputData();
            var dataArray = sourceDS.getPointData().getScalars() ||
                sourceDS.getPointData().getArrays()[0];
            var dataRange = dataArray.getRange();
            var minV = Math.max(0.0, edgeValue - 0.3) / 0.7;
            this.actor
                .getProperty()
                .setGradientOpacityMinimumValue(0, (dataRange[1] - dataRange[0]) * 0.2 * minV * minV);
            this.actor
                .getProperty()
                .setGradientOpacityMaximumValue(0, (dataRange[1] - dataRange[0]) * 1.0 * edgeValue * edgeValue);
            renderWindow.render();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('container'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], VolumeControlComponent.prototype, "container", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('width'),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], VolumeControlComponent.prototype, "setWidth", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('height'),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], VolumeControlComponent.prototype, "setHeight", null);
    VolumeControlComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'volume-control',
            template: __webpack_require__("./src/app/volume-control/volume-control.component.html"),
            styles: [__webpack_require__("./src/app/volume-control/volume-control.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], VolumeControlComponent);
    return VolumeControlComponent;
}());



/***/ }),

/***/ "./src/app/volume-control/volume-control.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VolumeControlModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__volume_control_component__ = __webpack_require__("./src/app/volume-control/volume-control.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__material_material_module__ = __webpack_require__("./src/app/material/material.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var VolumeControlModule = /** @class */ (function () {
    function VolumeControlModule() {
    }
    VolumeControlModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["a" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__material_material_module__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormsModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__volume_control_component__["a" /* VolumeControlComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_3__volume_control_component__["a" /* VolumeControlComponent */]]
        })
    ], VolumeControlModule);
    return VolumeControlModule;
}());



/***/ }),

/***/ "./src/app/volume-slicer/volume-slicer.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/volume-slicer/volume-slicer.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-list>\n <mat-list-item>\n \tSlice I\n \t<mat-slider (input)=\"updateSlide('i',$event)\" #slideI min=\"0\" max=\"5\" step=\"1\" value=\"1\"></mat-slider>\n </mat-list-item>\n <mat-list-item>\n \tSlide J\n \t<mat-slider (input)=\"updateSlide('j',$event)\" #slideJ min=\"0\" max=\"5\" step=\"1\" value=\"1\"></mat-slider>\n </mat-list-item>\n <mat-list-item>\n \tSlice K\n \t<mat-slider (input)=\"updateSlide('k',$event)\" #slideK min=\"0\" max=\"5\" step=\"1\" value=\"1\"></mat-slider>\n </mat-list-item>\n <mat-list-item>\n \tColor level\n \t<mat-slider (input)=\"updateCL($event)\" #slideCL min=\"0\" max=\"5\" step=\"1\" value=\"1\"></mat-slider>\n </mat-list-item>\n <mat-list-item>\n \tColor Window\n \t<mat-slider (input)=\"updateCW($event)\" #slideCW min=\"0\" max=\"5\" step=\"1\" value=\"1\"></mat-slider>\n </mat-list-item>\n</mat-list>\n"

/***/ }),

/***/ "./src/app/volume-slicer/volume-slicer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VolumeSlicerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mapper_ImageMapper__ = __webpack_require__("./src/app/mapper/ImageMapper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Rendering_Core_ImageSlice__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Core/ImageSlice/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var VolumeSlicerComponent = /** @class */ (function () {
    function VolumeSlicerComponent() {
        this.isEnabled = true;
        this.imageMapperI = __WEBPACK_IMPORTED_MODULE_2__mapper_ImageMapper__["a" /* default */].newInstance();
        this.imageMapperJ = __WEBPACK_IMPORTED_MODULE_2__mapper_ImageMapper__["a" /* default */].newInstance();
        this.imageMapperK = __WEBPACK_IMPORTED_MODULE_2__mapper_ImageMapper__["a" /* default */].newInstance();
        this.imageActorI = __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Rendering_Core_ImageSlice__["a" /* default */].newInstance();
        this.imageActorJ = __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Rendering_Core_ImageSlice__["a" /* default */].newInstance();
        this.imageActorK = __WEBPACK_IMPORTED_MODULE_3_vtk_js_Sources_Rendering_Core_ImageSlice__["a" /* default */].newInstance();
    }
    VolumeSlicerComponent.prototype.ngOnInit = function () {
        //this.imageMapperI.setISlice(30);
        //this.imageMapperJ.setJSlice(30);
        //this.imageMapperK.setKSlice(30);
        this.imageActorI.setMapper(this.imageMapperI);
        this.imageActorJ.setMapper(this.imageMapperJ);
        this.imageActorK.setMapper(this.imageMapperK);
    };
    VolumeSlicerComponent.prototype.enable = function () {
        this.isEnabled = true;
        if (!this.screenRenderer)
            return;
        var renderer = this.screenRenderer.getRenderer();
        renderer.addActor(this.imageActorI);
        renderer.addActor(this.imageActorJ);
        renderer.addActor(this.imageActorK);
        this.screenRenderer.getRenderWindow().render();
    };
    VolumeSlicerComponent.prototype.disable = function () {
        this.isEnabled = false;
        if (!this.screenRenderer)
            return;
        var renderer = this.screenRenderer.getRenderer();
        renderer.removeActor(this.imageActorI);
        renderer.removeActor(this.imageActorJ);
        renderer.removeActor(this.imageActorK);
        this.screenRenderer.getRenderWindow().render();
    };
    VolumeSlicerComponent.prototype.setDataSource = function (screenRenderer, source) {
        var _this = this;
        if (this.screenRenderer != screenRenderer) {
            this.screenRenderer = screenRenderer;
            if (this.isEnabled == true) {
                var renderer = screenRenderer.getRenderer();
                renderer.addActor(this.imageActorI);
                renderer.addActor(this.imageActorJ);
                renderer.addActor(this.imageActorK);
            }
        }
        this.imageMapperI.setInputData(source);
        this.imageMapperJ.setInputData(source);
        this.imageMapperK.setInputData(source);
        var dataRange = source
            .getPointData()
            .getScalars()
            .getRange();
        var extent = source.getExtent();
        var slideIds = ["i", "j", "k"];
        [this.slideI, this.slideJ, this.slideK].forEach(function (slider, index) {
            slider.min = extent[index * 2 + 0];
            slider.max = extent[index * 2 + 1];
            slider.value = (slider.min + slider.max) / 2;
            _this.updateSlide(slideIds[index], { value: slider.value, source: slider }, true);
        });
        [this.slideCL, this.slideCW].forEach(function (slider, index) {
            slider.max = dataRange[1];
            slider.value = dataRange[1];
        });
        this.slideCL.value = (dataRange[0] + dataRange[1]) / 2;
        this.updateCL({ value: this.slideCL.value, source: this.slideCL }, false);
        this.updateCW({ value: this.slideCW.value, source: this.slideCW }, false);
        //this.screenRenderer.getRenderer().resetCamera();
        //this.screenRenderer.getRenderWindow().render()
    };
    VolumeSlicerComponent.prototype.updateSlide = function (slideId, $event, render) {
        if (render === void 0) { render = true; }
        if (!this.screenRenderer)
            return;
        var slideValue = Math.floor($event.value);
        if (slideId == 'i')
            this.imageActorI.getMapper().setISlice(slideValue);
        if (slideId == 'j')
            this.imageActorJ.getMapper().setJSlice(slideValue);
        if (slideId == 'k')
            this.imageActorK.getMapper().setKSlice(slideValue);
        if (render == true)
            this.screenRenderer.getRenderWindow().render();
    };
    VolumeSlicerComponent.prototype.updateCL = function ($event, render) {
        if (render === void 0) { render = true; }
        if (!this.screenRenderer)
            return;
        var colorLevel = $event.value;
        this.imageActorI.getProperty().setColorLevel(colorLevel);
        this.imageActorJ.getProperty().setColorLevel(colorLevel);
        this.imageActorK.getProperty().setColorLevel(colorLevel);
        if (render == true)
            this.screenRenderer.getRenderWindow().render();
    };
    VolumeSlicerComponent.prototype.updateCW = function ($event, render) {
        if (render === void 0) { render = true; }
        if (!this.screenRenderer)
            return;
        var colorLevel = $event.value;
        this.imageActorI.getProperty().setColorWindow(colorLevel);
        this.imageActorJ.getProperty().setColorWindow(colorLevel);
        this.imageActorK.getProperty().setColorWindow(colorLevel);
        if (render == true)
            this.screenRenderer.getRenderWindow().render();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('slideI'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatSlider */])
    ], VolumeSlicerComponent.prototype, "slideI", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('slideJ'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatSlider */])
    ], VolumeSlicerComponent.prototype, "slideJ", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('slideK'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatSlider */])
    ], VolumeSlicerComponent.prototype, "slideK", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('slideCL'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatSlider */])
    ], VolumeSlicerComponent.prototype, "slideCL", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('slideCW'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatSlider */])
    ], VolumeSlicerComponent.prototype, "slideCW", void 0);
    VolumeSlicerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'volume-slicer',
            template: __webpack_require__("./src/app/volume-slicer/volume-slicer.component.html"),
            styles: [__webpack_require__("./src/app/volume-slicer/volume-slicer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], VolumeSlicerComponent);
    return VolumeSlicerComponent;
}());



/***/ }),

/***/ "./src/app/volume-slicer/volume-slicer.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VolumeSlicerModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__volume_slicer_component__ = __webpack_require__("./src/app/volume-slicer/volume-slicer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__material_material_module__ = __webpack_require__("./src/app/material/material.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var VolumeSlicerModule = /** @class */ (function () {
    function VolumeSlicerModule() {
    }
    VolumeSlicerModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_3__material_material_module__["a" /* MaterialModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__volume_slicer_component__["a" /* VolumeSlicerComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__volume_slicer_component__["a" /* VolumeSlicerComponent */]]
        })
    ], VolumeSlicerModule);
    return VolumeSlicerModule;
}());



/***/ }),

/***/ "./src/app/volume-viewer/volume-viewer.component.css":
/***/ (function(module, exports) {

module.exports = "\n.rContainer {\n\theight: 100%;\n\twidth: 100%;\n}"

/***/ }),

/***/ "./src/app/volume-viewer/volume-viewer.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"rContainer\" #container>\n\t\n</div>"

/***/ }),

/***/ "./src/app/volume-viewer/volume-viewer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VolumeViewerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__("./node_modules/rxjs/_esm5/operators.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_combineLatest__ = __webpack_require__("./node_modules/rxjs/_esm5/observable/combineLatest.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vtk_js_Sources_Rendering_Misc_GenericRenderWindow__ = __webpack_require__("./node_modules/vtk.js/Sources/Rendering/Misc/GenericRenderWindow/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_vtk_js_Sources_IO_XML_XMLImageDataReader__ = __webpack_require__("./node_modules/vtk.js/Sources/IO/XML/XMLImageDataReader/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var VolumeViewerComponent = /** @class */ (function () {
    function VolumeViewerComponent() {
        this.screenRenderer = __WEBPACK_IMPORTED_MODULE_4_vtk_js_Sources_Rendering_Misc_GenericRenderWindow__["a" /* default */].newInstance();
        this.sourceData = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["a" /* BehaviorSubject */](null);
        this.vCtrlSubject = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["a" /* BehaviorSubject */](null);
        this.sliceCtrlSubject = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["a" /* BehaviorSubject */](null);
    }
    Object.defineProperty(VolumeViewerComponent.prototype, "setSlideCtrl", {
        set: function (_sliceCtrl) {
            this.sliceCtrlSubject.next(_sliceCtrl);
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(VolumeViewerComponent.prototype, "setVCtrl", {
        set: function (_vCtrl) {
            this.vCtrlSubject.next(_vCtrl);
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(VolumeViewerComponent.prototype, "setFile", {
        set: function (_file) {
            var _this = this;
            if (!_file)
                return;
            var fileReader = new FileReader();
            fileReader.onload = function (ev) {
                var vtiReader = __WEBPACK_IMPORTED_MODULE_5_vtk_js_Sources_IO_XML_XMLImageDataReader__["a" /* default */].newInstance();
                vtiReader.parseAsArrayBuffer(fileReader.result);
                _this.sourceData.next(vtiReader.getOutputData(0));
            };
            fileReader.readAsArrayBuffer(_file);
        },
        enumerable: true,
        configurable: true
    });
    ;
    VolumeViewerComponent.prototype.ngOnInit = function () {
        var _this = this;
        var containerElm = this.container.nativeElement;
        var screenBackGround = [0, 0, 0];
        this.screenRenderer.setBackground(screenBackGround);
        this.screenRenderer.setContainer(containerElm);
        this.screenRenderer.resize();
        var renderer = this.screenRenderer.getRenderer();
        var renderWindow = this.screenRenderer.getRenderWindow();
        renderWindow.getInteractor().setDesiredUpdateRate(15);
        renderer.resetCamera();
        /*
    
        let lookupTable = vtkColorTransferFunction.newInstance();
        let piecewiseFunction = vtkPiecewiseFunction.newInstance();
    
    
        // Pipeline handling
        this.actor.setMapper(this.mapper);
        renderer.addActor(this.actor);
    
    
        //Configuration
        this.actor.getProperty().setRGBTransferFunction(0, lookupTable);
        this.actor.getProperty().setScalarOpacity(0, piecewiseFunction);
        this.actor.getProperty().setInterpolationTypeToLinear();
        this.actor.getProperty().setGradientOpacityMinimumValue(0, 0);
    
        this.actor.getProperty().setShade(true);
        this.actor.getProperty().setUseGradientOpacity(0, true);
          // - generic good default
        this.actor.getProperty().setGradientOpacityMinimumOpacity(0, 0.0);
        this.actor.getProperty().setGradientOpacityMaximumOpacity(0, 1.0);
        this.actor.getProperty().setAmbient(0.2);
        this.actor.getProperty().setDiffuse(0.7);
        this.actor.getProperty().setSpecular(0.3);
        this.actor.getProperty().setSpecularPower(8.0);
    
      */
        var notNullFilter = Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["a" /* filter */])(function (s) { return s != null; });
        Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_combineLatest__["a" /* combineLatest */])(this.sourceData.pipe(notNullFilter), this.vCtrlSubject.pipe(notNullFilter))
            .subscribe(function (params) {
            var source = params[0];
            var vCtrl = params[1];
            vCtrl.setDataSource(_this.screenRenderer, source);
        });
        Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_combineLatest__["a" /* combineLatest */])(this.sourceData.pipe(notNullFilter), this.sliceCtrlSubject.pipe(notNullFilter))
            .subscribe(function (params) {
            var source = params[0];
            var slicerCtrl = params[1];
            slicerCtrl.setDataSource(_this.screenRenderer, source);
        });
        this.sourceData.pipe(notNullFilter).subscribe(function (source) { return _this.updateSource(source); });
    };
    VolumeViewerComponent.prototype.updateSource = function (source) {
        var _this = this;
        /*
        console.log("source:", source);
    
    
        let dataArray = source.getPointData().getScalars() || source.getPointData().getArrays()[0];
        let dataRange = dataArray.getRange();
    
        this.mapper.setInputData(source);
    
        let sampleDistance = 0.7 * Math.sqrt(source.getSpacing().map((v) => v * v).reduce((a, b) => a + b, 0));
          this.mapper.setSampleDistance(sampleDistance);
          this.actor.getProperty().setScalarOpacityUnitDistance(0,vtkBoundingBox.getDiagonalLength(source.getBounds()) /Math.max(...source.getDimensions()));
          this.actor.getProperty().setGradientOpacityMaximumValue(0, (dataRange[1] - dataRange[0]) * 0.05)
      
          */
        this.screenRenderer.getRenderer().resetCamera();
        this.screenRenderer.getRenderWindow().render();
        setTimeout(function () { return _this.updateSource(source); }, 330);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('sliceCtrl'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], VolumeViewerComponent.prototype, "setSlideCtrl", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('vCtrl'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], VolumeViewerComponent.prototype, "setVCtrl", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('container'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], VolumeViewerComponent.prototype, "container", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Input */])('file'),
        __metadata("design:type", File),
        __metadata("design:paramtypes", [File])
    ], VolumeViewerComponent.prototype, "setFile", null);
    VolumeViewerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'volume-viewer',
            template: __webpack_require__("./src/app/volume-viewer/volume-viewer.component.html"),
            styles: [__webpack_require__("./src/app/volume-viewer/volume-viewer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], VolumeViewerComponent);
    return VolumeViewerComponent;
}());



/***/ }),

/***/ "./src/app/volume-viewer/volume-viewer.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VolumeViewerModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__volume_viewer_component__ = __webpack_require__("./src/app/volume-viewer/volume-viewer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__material_material_module__ = __webpack_require__("./src/app/material/material.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var VolumeViewerModule = /** @class */ (function () {
    function VolumeViewerModule() {
    }
    VolumeViewerModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["a" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__material_material_module__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormsModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__volume_viewer_component__["a" /* VolumeViewerComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_3__volume_viewer_component__["a" /* VolumeViewerComponent */]]
        })
    ], VolumeViewerModule);
    return VolumeViewerModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs__ = __webpack_require__("./node_modules/hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_hammerjs__);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map